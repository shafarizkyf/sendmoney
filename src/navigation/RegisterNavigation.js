import { createStackNavigator } from 'react-navigation';
import { UserBiodata, UserVerify, UserAddressAndSecurity } from '../pages';

const RegisterNavigation = createStackNavigator({
  'register':{
    screen: UserBiodata,
  },
  'verify':{
    screen: UserVerify,
  },
  'addressAndSecurity':{
    screen: UserAddressAndSecurity,
  },
}, {
  initialRouteName: 'register',
  headerMode: 'none',
  cardStyle: {backgroundColor: 'white'}
});

export default RegisterNavigation;