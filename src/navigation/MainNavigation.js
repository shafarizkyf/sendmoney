import React from 'react';
import {
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';
import { 
  createDrawerNavigator,
} from 'react-navigation';
import {
  Login,
  Home,
  Pricing,
  Feature,
  Service,
  PrivacyPolicy,
  TermsAndConditions,
  AboutUs,
} from '../pages';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import languages from '../config/languages';
import StackNavigation from './StackNavigation';
import RegisterNavigation from './RegisterNavigation';
import ForgetPasswordNavigator from './ForgetPasswordNavigator';

const CustomDrawerComponent = (props) => {
  return (
    <View style={[mystyles.page]}>
      <View style={[mystyles.mt10]}>
        <Image source={ require('../../assets/img/bg-drawer.png') } style={ styles.drawerBg } />
      </View>
      <ScrollView>
        <CustomDrawerItems {...props} />
      </ScrollView>
    </View>
  )
};

const CustomDrawerItems = (props) => {
  return (
    <View style={[mystyles.page]}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={[mystyles.mh20, mystyles.mt20]}>

          {
            //if not logged in
            !props.screenProps.isLoggedIn &&
            <TouchableOpacity onPress={ () => { props.navigation.navigate('login') } }>
              <Text style={[styles.item]}>
                { languages.drawer.login[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>
          }

          {
            //if not logged in
            !props.screenProps.isLoggedIn &&
            <TouchableOpacity onPress={ () => { props.navigation.navigate('register', {updateData: false}) } }>
              <Text style={[styles.item]}>
                { languages.drawer.register[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>
          }

          {
            //if logged in
            props.screenProps.isLoggedIn &&
            <TouchableOpacity onPress={ () => { props.navigation.navigate('menu') } }>
              <Text style={[styles.item]}>
                Menu
              </Text>
            </TouchableOpacity>
          }

          {
            props.screenProps.isLoggedIn &&
            <TouchableOpacity onPress={ () => { 
              props.screenProps.isLoggedIn = false;
              props.navigation.navigate('home');
              props.navigation.closeDrawer();
             }}>
              <Text style={[styles.item]}>
                { languages.drawer.logout[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>
          }

          <View style={{marginTop:60}}>
            <TouchableOpacity onPress={() => props.navigation.navigate('termsAndConditions') }>
              <Text style={[styles.item, mystyles.f12]}>
                { languages.drawer.termsAndCondition[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => props.navigation.navigate('privacyPolicy') }>
              <Text style={[styles.item, mystyles.f12]}>
                { languages.drawer.privacyPolicy[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => props.navigation.navigate('aboutUs') }>
              <Text style={[styles.item, mystyles.f12]}>
                { languages.drawer.aboutUs[props.screenProps.selectedLanguage] }
              </Text>
            </TouchableOpacity>
          </View>
        </View>    
      </ScrollView>

      <View style={[mystyles.page, mystyles.mh20, mystyles.mt20]}>
        <View style={[mystyles.row, {justifyContent:'flex-end'}]}>
          <Text style={[mystyles.mr10, mystyles.textDarkGrey]}>{ languages.drawer.language[props.screenProps.selectedLanguage] }</Text>
          <TouchableOpacity onPress={ () => { props.screenProps.onLanguageChange('id') } }>
            <Image source={ require('../../assets/img/indonesia-flag.png') } style={[mystyles.iconFlag20, mystyles.mr10]} />
          </TouchableOpacity>
          <TouchableOpacity onPress={ () => { props.screenProps.onLanguageChange('en') } }>
            <Image source={ require('../../assets/img/english-flag.png') } style={[mystyles.iconFlag20]} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const MainNavigation = createDrawerNavigator({
  'home':{
    screen: Home,
    navigationOptions: {
      title: 'Beranda',
      drawerLabel: () => null
    }
  },
  'service':{
    screen: Service,
    navigationOptions: {
      title: 'Layanan',
      drawerLabel: () => null
    }
  },
  'pricing':{
    screen: Pricing,
    navigationOptions: {
      title: 'Biaya',
      drawerLabel: () => null,
    }
  },
  'feature':{
    screen: Feature,
    navigationOptions: {
      title: 'Fitur',
      drawerLabel: () => null,
    }
  },
  'login':{
    screen: Login,
    navigationOptions: {
      title: 'Login',
      drawerLabel: () => null,
    }
  },
  forgetPassword:{
    screen: ForgetPasswordNavigator,
    navigationOptions:{
      title: 'Lupa Password',
      drawerLabel: () => null,
    }
  },
  'privacyPolicy':{
    screen: PrivacyPolicy,
    navigationOptions: {
      title: 'Kebijakan Privasi',
      drawerLabel: () => null
    }
  },
  'termsAndConditions':{
    screen: TermsAndConditions,
    navigationOptions: {
      title: 'Syarat dan Ketentuan',
      drawerLabel: () => null
    }
  },
  'aboutUs':{
    screen: AboutUs,
    navigationOptions: {
      title: 'About Us',
      drawerLabel: () => null
    }
  },
  registration: RegisterNavigation,
  main: StackNavigation
}, {
  initialRouteName: 'home',
  contentComponent: CustomDrawerComponent
})

const styles = StyleSheet.create({
  drawerBg:{
    width: 300,
    height: 165
  },
  item:{
    fontSize: 16,
    color: colors.black,
    paddingVertical: 10,
    marginBottom: 5,
  } 
});

export default MainNavigation;