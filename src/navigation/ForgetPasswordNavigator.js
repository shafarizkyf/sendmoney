import { createStackNavigator } from 'react-navigation';
import { 
  ForgetPassword,
  Login
} from '../pages';

const ForgetPasswordNavigator = createStackNavigator({
  'login':{
    screen: Login,
  },
  'forgetPassword':{
    screen: ForgetPassword,
  },
}, {
  initialRouteName: 'forgetPassword',
  headerMode: 'none',
  cardStyle: {backgroundColor: 'white'}
});

export default ForgetPasswordNavigator;