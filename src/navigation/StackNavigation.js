import { createStackNavigator } from 'react-navigation';
import {
  UserBiodata,
  UserVerifyIdentity,
  Setting,
  Profile,
  Mutation,
  Withdraw,
  Send,
  Deposit,
  Exchange,
  MainMenu,
  UserAddress,
  UserVerifyPhoneNumber,
  Currency,
  Bank,
  Security,
  UserPassword,
  AddBank,
  SendDetail,
  Notification,
} from '../pages';

const StackNavigation = createStackNavigator({
  'menu':{
    screen: MainMenu,
  },
  'biodata':{
    screen: UserBiodata,
  },
  'exchange':{
    screen: Exchange,
  },
  'deposit':{
    screen: Deposit,
  },
  'send':{
    screen: Send,
  },
  'withdraw':{
    screen: Withdraw,
  },
  'mutation':{
    screen: Mutation,
  },
  'profile':{
    screen: Profile,
  },
  'setting':{
    screen: Setting,
  },
  'verifyIdentity':{
    screen: UserVerifyIdentity,
  },
  'address':{
    screen: UserAddress,
  },
  'verifyPhoneNumber':{
    screen: UserVerifyPhoneNumber,
  },
  'currency':{
    screen: Currency,
  },
  'bank':{
    screen: Bank,
  },
  'security':{
    screen: Security,
  },
  'userPassword':{
    screen: UserPassword,
  },
  'addBank':{
    screen: AddBank,
  },
  'sendDetail':{
    screen: SendDetail,
  },
  'notification':{
    screen: Notification,
  },
}, {
  initialRouteName: 'menu',
  headerMode: 'none',
  cardStyle: {backgroundColor:'white'}
});

export default StackNavigation;