import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  TableHead,
  TableBody,
} from '../components';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';

export default class Bank extends React.Component {
  
  state = {
    tableHeader: [
      languages.page.bank.tableHeader.code[this.props.screenProps.selectedLanguage], 
      languages.page.bank.tableHeader.name[this.props.screenProps.selectedLanguage], 
      languages.page.bank.tableHeader.currency[this.props.screenProps.selectedLanguage], 
      languages.page.bank.tableHeader.account[this.props.screenProps.selectedLanguage], 
      languages.page.bank.tableHeader.user[this.props.screenProps.selectedLanguage], 
      languages.page.bank.tableHeader.status[this.props.screenProps.selectedLanguage]
    ],
    tableBody: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    this.userBanks();
  }

  userBanks(){
    const data = {
      com: api.com.getUserBank,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    console.log('user banks -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('user banks -->', response);

      let userBanks = [];
      if(response.resultCode == api.response.success){
        userBanks = response.data.map(obj => {
          return [
            obj.kode_bank,
            obj.nama_bank,
            obj.mata_uang,
            obj.nomor_rekening,
            obj.nama_nasabah,
            obj.status == "true"
              ? languages.page.bank.tableHeader.active[this.state.language] 
              : languages.page.bank.tableHeader.inactive[this.state.language] 
          ]
        });
        
        this.setState({tableBody: userBanks});
      }

    }).catch(error => {
      console.log('error user banks -->', error);
    });
  }

  render(){
    return (
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.menu.bank[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <View style={[mystyles.mt10]}></View>
        <TouchableOpacity activeOpacity={0.8} onPress={() => {this.props.navigation.navigate('addBank')}}>
          <Text style={[styles.button]}>
            { languages.page.bank.tableHeader.addBank[this.state.language]  }
          </Text>
        </TouchableOpacity>
        <ScrollView>          
          <View>
            <TableHead data={this.state.tableHeader} />
            {
              this.state.tableBody.map((obj, index) => <TableBody key={index} data={obj} index={index} />)
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button:{
    color: 'white',
    backgroundColor: colors.moss,
    paddingVertical: 5,
    paddingHorizontal: 10,
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
    marginLeft: 20,
    marginBottom: 10,
    borderRadius: 3,
    fontSize: 12,
  }
})