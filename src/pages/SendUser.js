import React from 'react';
import { 
  View, 
  Text, 
  ScrollView, 
  TouchableOpacity, 
  KeyboardAvoidingView 
} from 'react-native';
import { 
  CurrencyOption, 
  MyTextInput 
} from '../components';
import mystyles from '../styles/mystyles';
import flag from '../config/flag';
import languages from '../config/languages';

export default class SendUser extends React.Component {

  state = {
    currencyList: [{value: '', label: languages.blankOptions.currency[this.props.language]}]
  };

  componentDidMount(){
    if(this.props.currency){
      this.setState({currencyList: this.props.currency});
    }
    
    if(this.props.user){
      this.setState({from: this.props.user.id});
    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>
        <ScrollView style={[mystyles.pb20]} showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20, mystyles.mt10]}>

            <MyTextInput 
              label={ languages.page.send.form.sendUser.from[this.props.language] }
              isRequiredMark={ true }
              value={ this.props.user.id }
              onChangeText={ (from) => this.setState({from}) }
              error={ this.props.errors.from }
              disabled />

            <MyTextInput 
              label={ languages.page.send.form.sendUser.to[this.props.language] } 
              isRequiredMark={ true }
              value={ this.state.to }
              onChangeText={ (to) => this.setState({to}) }
              error={ this.props.errors.to } />

            <CurrencyOption
              text={ languages.page.send.form.sendUser.currency[this.props.language] }
              onChange={ (currency, position) => this.setState({currency, flag: this.state.currencyList[position].flag}) }
              value={ this.state.currency }
              flag={ this.state.flag }
              error={ this.props.errors.currency }
              items={ this.state.currencyList } />

            <MyTextInput 
              label={ languages.page.send.form.sendUser.amount[this.props.language] } 
              isRequiredMark={ true }
              value={ this.state.amount }
              onChangeText={ (amount) => { this.setState({amount}) } }
              keyboardType="numeric" 
              error={ this.props.errors.amount } />
      
            <MyTextInput 
              label={ languages.page.send.form.sendUser.note[this.props.language] } 
              value={this.state.note}
              onChangeText={ (note) => { this.setState({note}) } }
              error={ this.props.errors.note } />

          </View>
        </ScrollView>

        <TouchableOpacity style={[]} activeOpacity={0.8} onPress={() => { this.props.onProses(this.state) }}>
          <Text style={mystyles.formButton}>
            { languages.page.send.form.sendUser.proceed[this.props.language] }
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
  
}