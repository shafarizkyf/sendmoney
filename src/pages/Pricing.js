import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  ScrollView
} from 'react-native';
import {
  Navbar,
  TopMenu, 
  Footer,
  FeatureTableHeader,
  FeatureTableBody,
} from '../components';
import colors from '../config/colors';
import language from '../config/languages';
import mystyle from '../styles/mystyles';

export default class Pricing extends React.Component {

  state = {
    language: 'id',
    tableHead: ['Layanan', 'Aktif', 'Terverifikasi'],
    tableBody: [
      ['Pendaftaran', 'Gratis', 'Gratis'],
      ['Konversi Mata Uang', false, 'Disesuaikan Nilai Kurs'],
      ['Berlangganan Sistem ', 'Gratis', 'Gratis'],
      ['Deposit Ke Akun SendMoney', 'Gratis', 'Gratis'],
      ['Transfer Antar Akun SendMoney', '0.5%', '0.5%'],
      ['Transfer Ke Bank Nasional', false, '0.5%'],
      ['Transfer Ke Bank Internasional', false, '0.5%'],
      ['Penarikan Ke Rekening Sesama Bank', false, 'Gratis'],
      ['Penarikan Ke Bank Lain Dalam Neger', 'Rp 10.000', 'Rp 10.000'],
      ['Penarikan Ke Bank Lain Luar Neger', '0.5%', '0.5%'],
      ['API SendMoney', false, 'Gratis'],
    ]
  }
  
  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  render(){
    return (
      <View style={ mystyle.page }>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />

        <TopMenu 
          homePage={ () => this.props.navigation.navigate('home') }
          servicePage={ () => this.props.navigation.navigate('service') }
          featurePage={ () => this.props.navigation.navigate('feature') }
          pricingPage={ () => this.props.navigation.navigate('pricing') }
          language={ this.state.language } selected={ 4 } />
        
        <View style={[mystyle.page]}>
          <ScrollView>
            <View style={[mystyle.mh20]}>
              <Text style={ mystyle.pageTitle }>{ language.home.navbarMenu.pricing[this.state.language] }</Text>
              <View style={ mystyle.borderGrey }>             
                <FeatureTableHeader data={this.state.tableHead} />
                { this.state.tableBody.map((obj, index) =>  <FeatureTableBody key={index} data={obj} index={index} />) }
              </View>
            </View>
          </ScrollView>          
          <Footer />
        </View>

      </View>
    );
  }
}
