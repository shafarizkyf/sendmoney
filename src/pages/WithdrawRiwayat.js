import React from 'react';
import { View, ScrollView } from 'react-native';
import mystyles from '../styles/mystyles';
import { DepositRiwayatCard } from '../components';

const WithdrawRiwayat = (props) => (
  <View style={[ mystyles.page, mystyles.bgGrey ]}>
    <ScrollView showsVerticalScrollIndicator={false}>
      { props.history.map((obj,index) => <DepositRiwayatCard key={index} data={obj} type="proccess" />) }
    </ScrollView>
  </View>
)

export default WithdrawRiwayat;