import React, { Component } from 'react';
import { 
  View, 
  Text, 
  ScrollView, 
  TouchableOpacity, 
  KeyboardAvoidingView 
} from 'react-native';
import { 
  CurrencyOption, 
  MyTextInput, 
  MySelectInput 
} from '../components';
import mystyles from '../styles/mystyles';
import flag from '../config/flag';
import languages from '../config/languages';

export default class DepositOtomatis extends Component {

  state = {
    banks: [{value: '', label: languages.blankOptions.bank[this.props.language]}],
    methodList: [{value: '', label: languages.blankOptions.paymentMethod[this.props.language]}],
    currencyList: [{value: '', label: languages.blankOptions.currency[this.props.language]}],
  };

  componentDidMount(){
    if(this.props.banks){
      this.setState({banks: this.props.banks});
    }
    
    if(this.props.currency){
      this.setState({currencyList: this.props.currency});
    }

    if(this.props.methodList){
      this.setState({methodList: this.props.methodList});
    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20, mystyles.mt10]}>

            <CurrencyOption 
              onChange={(currency, position) => { this.setState({currency, flag: this.state.currencyList[position].flag})}} 
              value={ this.state.currency }
              flag={ this.state.flag }
              error={ this.props.errors.currency }
              items={ this.state.currencyList }
              text={ languages.page.deposit.form.depositManual.currency[this.props.language] } />

            <MySelectInput 
              label={ languages.page.deposit.form.depositManual.depositMethod[this.props.language] }
              items={ this.state.methodList }
              isRequiredMark={true}
              onChangeSelect={ (val) => { this.setState({method:val}) } }
              value={ this.state.method }
              error={ this.props.errors.method } />
      
            <MyTextInput 
              label={ languages.page.deposit.form.depositManual.amount[this.props.language] }
              isRequiredMark={true}
              value={this.state.amount}
              onChangeText={ (val) => { this.setState({amount:val}) } }
              keyboardType="numeric" 
              error={ this.props.errors.amount } />
      
            <MyTextInput 
              label={ languages.page.deposit.form.depositManual.note[this.props.language] } 
              isRequiredMark={true}
              value={this.state.note}
              onChangeText={ (val) => { this.setState({note:val}) } }
              error={ this.props.errors.note } />

          </View>
        </ScrollView>
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.onProses(this.state) }>
          <Text style={mystyles.formButton}>{ languages.page.deposit.form.depositManual.proceed[this.props.language] }</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
  
}