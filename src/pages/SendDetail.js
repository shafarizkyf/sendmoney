import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native';
import {
  Navbar,
  Breadcrumb,
  Balance,
  Tabs
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';

export default class SendDetail extends Component{
  render(){
    return(
      <View style={[mystyles.page, mystyles.bgGrey]}>
        <ScrollView>
          <View>
            <View style={[mystyles.mh20]}>
              <Text style={[mystyles.mb10, mystyles.mt10, mystyles.f18]}>Transfer Berhasil</Text>
              <View style={[]}>
                <Text style={[mystyles.bgOrange, mystyles.textWhite,{padding:10}]}>Informasi Transfer</Text>
                <View style={[mystyles.row, mystyles.bgYellow]}>
                  <Text style={styles.w40}>Nomor Referensi</Text>
                  <Text style={styles.w60}>1900009082523</Text>
                </View>
                <View style={[mystyles.row, mystyles.bgLightYellow]}>
                  <Text style={styles.w40}>Dari</Text>
                  <Text style={styles.w60}>Junaidi - M000912</Text>
                </View>
                <View style={[mystyles.row, mystyles.bgYellow]}>
                  <Text style={styles.w40}>Ke</Text>
                  <Text style={styles.w60}>John Doe - M00234</Text>
                </View>
                <View style={[mystyles.row, mystyles.bgLightYellow]}>
                  <Text style={styles.w40}>Jumlah</Text>
                  <Text style={styles.w60}>Rp 100.000</Text>
                </View>
                <View style={[mystyles.row, mystyles.bgYellow]}>
                  <Text style={styles.w40}>Keterangan</Text>
                  <Text style={styles.w60}>-</Text>
                </View>
                <View style={[mystyles.row, mystyles.bgLightYellow]}>
                  <Text style={styles.w40}>Tanggal</Text>
                  <Text style={styles.w60}>25 Oktober 2018 - 09:59:10</Text>
                </View>
              </View>

              <View style={[mystyles.mt20, mystyles.page]}>
                <TouchableOpacity>
                  <Text style={[styles.btnOutlineSuccess, mystyles.selfCenter, mystyles.mb10]}>Download</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                  <Text style={[styles.btnOutlineSuccess, mystyles.selfCenter, mystyles.mb10]}>Bagikan</Text>
                </TouchableOpacity>
              </View>

            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  w40:{
    width:'40%',
    padding:10
  },
  w60:{
    width:'60%',
    padding:10
  },
  btnOutlineSuccess:{
    color: colors.green,
    borderWidth: 1,
    borderColor: colors.green,
    borderRadius: 50,
    fontSize: 18,
    padding: 10,
    textAlign: 'center',
    width: 200
  }
});