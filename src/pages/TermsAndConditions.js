import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Navbar } from '../components';
import mystyles from '../styles/mystyles';

export default class TermsAndConditions extends React.Component {

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />
        <View style={[mystyles.navbarBorder]}></View>
        <View style={[mystyles.page]}>
          <ScrollView>
            <Text style={[mystyles.pageTitle, mystyles.mh20]}>Syarat dan Ketentuan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Dengan menjadi Anggota SendMoney, maka Anda dianggap telah mengetahui dan menyetujui untuk terikat dengan syarat dan ketentuan yang dikeluarkan oleh pihak SendMoney. Apabila Anda tidak menyetujui syarat dan ketentuan ini, Anda diharapkan untuk berhenti menggunakan layanan sebagai anggota SendMoney. Layanan sebagai anggota SendMoney hanya terbatas pada pelanggan yang mempunyai akun SendMoney. Anggota SendMoney bertanggungjawab penuh terhadap akses dan password yang digunakan. Pelanggaran terhadap Syarat dan Ketentuan ini merupakan perbuatan melawan hukum yang didasarkan pada hukum yang berlaku di Indonesia atau Undang-Undang serta peraturan yang mengatur.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>SendMoney berharap anggota SendMoney memperhatikan poin-poin berikut:</Text>
            
            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Istilah dan Definisi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	SendMoney adalah produk yang dikeluarkan oleh PT SendMoney yang bergerak di bidang jasa keuangan untuk transfer ke bank local maupun internasional.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2.	SendMoney.com adalah nama domain yang dipergunakan untuk menggunakan Layanan Keanggotaan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3.	Syarat dan ketentuan adalah bentuk kesepakatan antara Anggota SendMoney dengan SendMoney yang mengatur hak serta kewajiban antara kedua belah pihak yang menggunakan layanan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>4.	Anggota SendMoney dalam menggunakan layanan SendMoney dapat sebagai Personal SendMoney atau Merchant SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>5.	Personal SendMoney adalah pihak perseorangan yang menggunakan layanan SendMoney sebagai pembeli/konsumen.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>6.	Merchant SendMoney adalah pihak perseorangan dan/atau perusahaan yang menggunakan layanan SendMoney sebagai penjual, distributor atau produsen.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>7.	Akun SendMoney adalah identitas pengguna SendMoney untuk keperluan anggota SendMoney mengakses layanan, situs dan aplikasi SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>8.	Saldo SendMoney adalah sejumlah dana yang tersimpan pada akun anggota SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>9.	Transaksi adalah semua kegiatan finansial menyangkut saldo SendMoney milik anggota, termasuk dan tidak terkecuali: setor dana (deposit), kirim dana (transfer), tarik dana (withdraw) yang tersedia dalam situs atau aplikasi SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>10.	Data transfer adalah data yang tercantum pada mutasi bank, yang berisi nama, jumlah, berita dan keterangan lain dari pengirim dana.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>11.	Password adalah kombinasi huruf, angka, karakter khusus yang dibuat oleh anggota SendMoney untuk mengakses layanan, situs, akun dan aplikasi SendMoney dan bisa dirubah oleh anggota (pengguna) SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>12.	PIN (Personal Identification Number) SendMoney adalah nomor identifikasi pribadi yang bersifat rahasia dan hanya diketahui oleh anggota SendMoney serta harus dicantumkan/dimasukkan oleh anggota pada saat menggunakan layanan SendMoney. Bersama-sama dengan SendMoney ID, password dan PIN digunakan untuk membuktikan bahwa anggota bersangkutan adalah yang berhak atas layanan SendMoney.</Text>
            
            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Keanggotaan, Password dan Keamanan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	Akun SendMoney merupakan kode yang bersifat unik dan kewenangan penggunaannya ada pada anggota SendMoney. Akun SendMoney bersifat tetap.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2.	Layanan keanggotaan adalah layanan penyediaan aplikasi sistem SendMoney yang meliputi transaksi penyetoran dana (deposit) dan pengiriman dana (transfer) antar akun anggota serta penarikan dana (withdraw) ke rekening bank anggota SendMoney sesuai dengan limit transaksi. Limit transaksi telah ditentukan berdasarkan status keanggotaan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3.	Anggota SendMoney dengan ini menyatakan bahwa anggota SendMoney adalah orang yang dapat mempertanggungjawabkan seluruh perbuatannya sesuai dengan peraturan yang berlaku.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>4.	Seluruh anggota SendMoney dibebaskan dari biaya pendaftaran keanggotaan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>5.	Anggota SendMoney yang bertindak sebagai penjual disarankan memilih layanan Keanggotaan Merchant, sedangkan anggota SendMoney yang bertindak sebagai pembeli disarankan memilih layanan Keanggotaan Personal.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>6.	Anggota SendMoney wajib memberikan informasi yang benar dan akurat, termasuk tidak terbatas berisi nama lengkap, alamat, nomor telepon, nomor handphone, alamat email dan/atau alamat website, akun bank, dan tanggal lahir. Apabila anggota SendMoney memberikan informasi yang tidak benar, tidak tepat, atau tidak lengkap, maka SendMoney berhak untuk menangguhkan atau mengakhiri akun anggota SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>7.	Apabila diketahui dan dapat dibuktikan bahwa calon atau anggota SendMoney mengisi data diri dan/atau menggunakan identitas palsu maka SendMoney berhak membekukan akun tersebut beserta dana yang ada di dalamnya tanpa persetujuan terlebih dahulu.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>8.	Akun yang dibekukan oleh pihak SendMoney, seperti yang dijelaskan pada Pasal Keanggotaan, Password, dan Keamanan poin 7 di atas dapat diaktifkan kembali dengan syarat dan ketentuan sebagai berikut :</Text>
            
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>a.	Memberikan data diri dan data akun yang dibekukan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>b.	Mengunggah identitas diri yang asli dan masih berlaku.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>c.	Memberikan dan/atau mengunggah data lainnya yang diperlukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>d.	Permohonan pengaktifan akun dikirimkan melalui email menggunakan email yang terdaftar dan ditujukan kepada support@SendMoney.com</Text>
            
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>9.	Anggota SendMoney bertanggung jawab untuk menjaga kerahasiaan dan keamanan akun dan password anggota SendMoney tersebut, dan bertanggung jawab sepenuhnya atas segala kegiatan yang diatasnamakan nama akun anggota SendMoney tersebut.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>10. Anggota SendMoney bertanggung jawab untuk menjaga kerahasiaan dan keamanan email pribadi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>11. Anggota SendMoney diizinkan untuk merubah atau memperbaharui informasi keanggotaanya berkenaan dengan informasi pribadi dan/atau akun bank setiap saat. Anggota SendMoney juga diizinkan untuk menutup keanggotaannya secara permanen.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>12. SendMoney berhak sepenuhnya untuk membatasi, memblokir, atau mengakhiri pelayanan dari suatu akun dan mengambil tindakan atau langkah hukum apapun jika SendMoney menganggap anggota SendMoney melanggar hukum yang berlaku.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>13. SendMoney berhak untuk menutup akun anggota SendMoney baik sementara maupun untuk seterusnya apabila diduga adanya tindakan kecurangan atau tidak sesuai dengan Syarat dan Ketentuan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>14. SendMoney mempunyai hak penuh untuk menyetujui dan menolak saat proses verifikasi anggota SendMoney dan SendMoney juga berhak meminta data pendukung dalam proses verifikasi jika dirasa perlu.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>15. Anggota SendMoney menyetujui bahwa proses menjadi Anggota SendMoney hanya akan berlaku setelah seluruh persyaratan yang diminta SendMoney dipenuhi oleh anggota dan proses registrasi telah melalui proses verifikasi yang disetujui.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>16. Anggota SendMoney tidak diperbolehkan untuk mengalihkan akun kepada pihak lain dengan cara apapun tanpa sepengetahuan dan persetujuan tertulis dari SendMoney. Apabila Anggota SendMoney melanggar ketentuan ini, maka seluruh akibat dan risiko sepenuhnya merupakan tanggung jawab Anggota SendMoney sendiri yang mengalihkan akun.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>17. SendMoney menghimbau kepada Anggota SendMoney untuk tidak memberikan data penting kepada pihak lain atau pihak yang mengatasnamakan SendMoney yang tidak dapat dijamin keamanannya.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Kebijakan Privasi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>1. SendMoney menjamin kerahasiaan anggota SendMoney dari pihak lain. SendMoney tidak akan menjual, menukar, atau mengirimkan informasi yang diberikan oleh Anggota SendMoney kepada pihak ketiga yang tidak berkepentingan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>2. SendMoney dapat menyampaikan informasi yang didapat dari Anggota SendMoney, termasuk namun tidak terbatas pada permintaan dan/atau diperintahkan oleh hukum, memenuhi kepatuhan undang-undang yang berlaku, dari pihak berwajib, bank sentral, maupun lembaga keuangan negara lainnya.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>3. Anggota SendMoney tidak boleh mempergunakan keanggotaannya untuk kegiatan melanggar hukum internasional, melanggar hak-hak pihak lain atau kegiatan yang tidak sesuai dengan persyaratan dan ketentuan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>4. SendMoney berhak untuk menyimpan data percakapan anggota SendMoney tanpa persetujuan anggota SendMoney terlebih dulu.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Kewajiban dan Hak</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>1. Kewajiban Anggota SendMoney</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Anggota SendMoney diwajibkan untuk menjaga kerahasiaan identitasnya.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	Anggota SendMoney diwajibkan untuk mematuhi persyaratan dan ketentuan SendMoney.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Hak Anggota SendMoney</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Anggota SendMoney mendapatkan hak atas jaminan kerahasiaan informasi, terbatas pada Kebijakan Privasi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	Anggota SendMoney mendapatkan hak atas kelancaran semua transaksi SendMoney yang dilakukan sesuai dengan jam layanan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>c.	Anggota SendMoney mendapatkan hak atas layanan transaksi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>d.	Anggota SendMoney mendapatkan hak atas laporan riwayat transaksi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>e.	Anggota SendMoney mendapatkan hak atas keamanan dan jaminan saldo SendMoney, termasuk tidak terbatas yang disebabkan oleh kelalaian dan perangkat perusak.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>3.	Berdasarkan Kewajiban dan Hak Anggota SendMoney, maka :</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	SENDMONEY BERHAK MENON-AKTIFKAN AKUN YANG DISALAHGUNAKAN UNTUK PENYELIDIKAN LEBIH LANJUT TANPA PEMBERITAHUAN.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	SENDMONEY BERHAK MEMBEKUKAN AKUN YANG DIANGGAP BERMASALAH SECARA SEPIHAK.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>c.	SENDMONEY TIDAK BERTANGGUNG JAWAB ATAS AKSES ILEGAL TERHADAP AKUN ANGGOTA SENDMONEY. Semua transaksi yang sudah dilakukan tidak dapat dikembalikan. Pengiriman dana anggota SendMoney dijamin keamanannya sampai ke akun bank masing-masing.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>d.	SENDMONEY TIDAK PERNAH MEMINTA PASSWORD/PIN SENDMONEY. Setiap Anggota SendMoney wajib menjaga keamanan password/PIN masing-masing dengan tidak memberikan kepada orang lain. SendMoney tidak bertanggung jawab atas kerugian Anggota SendMoney yang dikarenakan penyalahgunaan password/PIN.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>e.	SENDMONEY MENGANGGAP BAHWA SEMUA ANGGOTA TELAH SETUJU UNTUK MENGGANTI RUGI ATAS SEMUA TINDAKAN KLAIM, KERUGIAN PERMINTAAN DAN/ATAU KERUSAKAN (TERMASUK BIAYA PENGACARA) YANG DIBUAT DAN/ATAU DIKELUARKAN OLEH PIHAK KETIGA YANG TIMBUL DARI DAN/ATAU TERKAIT DENGAN PENGGUNAAN LAYANAN.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>f.	SENDMONEY TIDAK BERTANGGUNG JAWAB ATAS SEMUA DETAIL TRANSAKSI ANTARA PENJUAL DENGAN PEMBELI DAN/ATAU PERTUKARAN INFORMASI ANTARA ANGGOTA SITUS INI. Oleh karena itu, SendMoney tidak bertanggung jawab apabila transaksi maupun pertukaran informasi yang tidak sesuai dan atau cacat secara hukum yang berlaku ataupun apabila terdapat konsekuensi hukum dari transaksi yang dilakukan melalui situs ini.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>g.	SENDMONEY TIDAK BERTANGGUNG JAWAB ATAS SEGALA AKIBAT YANG TERJADI SAAT DAN/ATAU SETELAH TRANSAKSI TERMASUK APABILA TERJADI KERUSAKAN PADA PRODUK DAN/ATAU BARANG DAN/ATAU JASA, GARANSI, BAGIAN – BAGIAN YANG TIDAK LENGKAP, PENURUNAN NILAI PRODUK DAN/ATAU BARANG DAN/ATAU JASA, MAUPUN KEWAJIBAN DAN KONSEKUENSI HUKUM YANG TERJADI KARENA TRANSAKSI.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>h.	SENDMONEY TIDAK MEMBUAT PERNYATAAN DAN/ATAU JAMINAN APAPUN SEHUBUNGAN DENGAN LAYANAN DAN/ATAU KINERJA LAYANAN PIHAK KETIGA, TERMASUK TIDAK TERBATAS PADA JAMINAN KELAYAKAN MERCHANT ATAU KETERSESUAIAN UNTUK TUJUAN TERTENTU.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>i.	SENDMONEY TIDAK BERTANGGUNG JAWAB PADA ANGGOTA ATAS KERUGIAN INSIDENTIL ATAU AKIBAT YANG TIMBUL DARI PELANGGARAN ATAS KERUGIAN INSIDENTIL ATAU AKIBAT YANG TIMBUL DARI PELANGGARAN ATAS PEMBAYARAN DAN/ATAU KONTRAK, KESALAHAN PENGIRIMAN, KETERLAMBATAN PENGIRIMAN, PENGIRIMAN YANG TIDAK SESUAI DENGAN KONDISI YANG DIKONFIRMASIKAN, PENGGUNAAN LAYANAN DAN/ATAU PRODUK, DAN SETIAP PELANGGRAN LAIN ATAS KONTRAK DAN/ATAU KEWAJIBAN ANTARA ANGGOTA SENDMONEY.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>j.	SENDMONEY BERHAK UNTUK TIDAK TERLIBAT DALAM PROSES HUKUM APABILA KESALAHPAHAMAN ANTARA PIHAK YANG BERSANGKUTAN DISELESAIKAN SECARA BERTAHAP DAN DIPROSES SECARA HUKUM (BILA DIPERLUKAN).</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>k.	SENDMONEY TIDAK BERTANGGUNG JAWAB ATAS PENYALAHGUNAAN TRANSAKSI TERMASUK TIDAK TERBATAS PENCUCIAN UANG, PEMBELIAN BARANG – BARANG TERLARANG, SENJATA API, PEMBIAYAAN TERORISME.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>l.	SENDMONEY BERHAK UNTUK DIBEBASKAN DARI SEMUA TUNTUTAN HUKUM YANG MUNGKIN TIMBUL ATAS PENGGUNAAN SENDMONEY SEBAGAI SISTEM PEMBAYARAN ONLINE.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>m.	SendMoney berhak untuk melakukan penundaan dan/atau melakukan proses investigasi atas proses Transaksi dari Anggota SendMoney yang dianggap menyimpang dari profil, karakteristik, atau kebiasaan pola Transaksi dari penggunaan Layanan SendMoney</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Transaksi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	Informasi yang dibutuhkan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>SendMoney akan membutuhkan informasi-informasi berikut dari para anggota SendMoney: : Nama Lengkap, Alamat, Nomor Telepon, Nomor Handphone, email, alamat website, tanggal lahir, dan/atau akun bank yang sebenar-benarnya.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Informasi Transaksi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Semua transaksi di SendMoney (Penyetoran dana/deposit, Pengiriman dana/transfer, Penarikan dana/withdrawal) yang mengaktifkan PIN transaksi membutuhkan verifikasi yang akan dikirim ke email dan/atau telepon seluler masing- masing anggota SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	Semua anggota SendMoney wajib memperhatikan jam layanan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>c.	SendMoney menyediakan riwayat transaksi selama 6 (enam) bulan terakhir. Untuk permintaan riwayat transaksi lebih dari 6 (enam) bulan terakhir, anggota SendMoney diwajibkan mengajukan permohonan khusus yang dikirim melalui email yang sudah resmi didaftarkan di akun anggota SendMoney.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>3.	Penyetoran Dana (Deposit)</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Anggota SendMoney yang melakukan penyetoran dana (deposit) wajib untuk mengisi form deposit dan mengirim dana menggunakan nomor rekening pribadi sesuai dengan jumlah yang tertera di form.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	Untuk proses identifikasi transaksi, Anggota SendMoney disarankan mencantumkan nomor referensi di berita transfer untuk proses identifikasi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>c.	SendMoney tidak akan memproses apabila Anggota SendMoney tidak menggunakan nomor rekening pribadi dan tidak terdapat nomor referensi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>d.	SendMoney tidak akan bertanggung jawab apabila transaksi deposit tidak diproses dikarenakan Anggota SendMoney tidak membuat form deposit terlebih dahulu dan/atau tidak mencantumkan nomor referensi.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>e.	SendMoney berhak meminta bukti transfer kepada Anggota SendMoney untuk keperluan pemrosesan deposit (bila diperlukan).</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>4.	Pengiriman Dana (Transfer)</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Pihak pengirim dana wajib memastikan terlebih dahulu akun SendMoney penerima dana yang dituju untuk mencegah terjadinya kesalahan pengiriman dana.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	<Text style={[mystyles.bold]}>SENDMONEY TIDAK BERTANGGUNGJAWAB ATAS SEMUA TRANSAKSI YANG DILAKUKAN OLEH PEMILIK AKUN SENDMONEY,</Text> yang meliputi:</Text>
            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold, mystyles.ml40]}>i.	Kelalaian</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml50]}>Adalah tindakan tanpa sengaja atau sengaja yang dilakukan pemilik akun SendMoney (salah memasukan akun SendMoney dan/atau salah memasukan jumlah nilai dalam transaksi).</Text>
            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold, mystyles.ml40]}>ii.	Penggunaan Oleh Pihak Lain</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml50]}>Adalah akun SendMoney yang digunakan bukan oleh pemiliknya sendiri termasuk tidak terbatas pada peretasan, penggunaan oleh teman, saudara, dan keluarga.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>c.	SENDMONEY BERHAK MELAKUKAN AUDIT DALAM PROSES PENGIRIMAN DANA ANTAR ANGGOTA SENDMONEY.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>5.	Penarikan Dana (Withdraw)</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>a.	Anggota SendMoney yang melakukan penarikan dana (withdraw) wajib untuk mengisi form penarikan dan menarik dana sesuai dengan jumlah yang tertera di form.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>b.	Anggota SendMoney yang melakukan penarikan dana diwajibkan untuk menggunakan data bank pribadi yang masih aktif ketika mengisi form penarikan. SendMoney akan membatalkan transaksi penarikan dana apabila data bank anggota tidak sesuai dengan data yang diisi di form penarikan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>c.	Batas transaksi penarikan dana adalah sebesar RP. 100.000.000,- (seratus juta rupiah) yang dilakukan baik dalam satu kali transaksi maupun beberapa kali transaksi dalam 1 (satu) hari kerja.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Penutupan Akun</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	Penonaktifan Akun</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>SendMoney berhak menonaktifkan akun SendMoney yang sudah tidak aktif selama 6 (enam) bulan berturut-turut.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2.	Pengembalian Hak Anggota</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>a.	SendMoney akan membantu mengirimkan dana yang terdapat di Akun SendMoney yang sudah tidak ada aktivitas (tidak aktif) selama 6 (enam) bulan berturut – turut ke rekening bank yang terdaftar di akun Anggota SendMoney tersebut dengan pemberitahuan terlebih dahulu kepada Anggota SendMoney tersebut selama 3 (tiga) kali pemberitahuan selama 30 (tiga puluh) hari.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>b.	Dalam hal sumber dana yang terdapat di Akun SendMoney yang sudah tidak ada aktivitas (tidak aktif) selama 6 (enam) bulan berturut – turut dan sumber dana berasal dari sengketa antara Anggota SendMoney, SendMoney akan membantu mengirimkan dana tersebut kepada akun Anggota SendMoney yang sesuai dengan riwayat transaksi, dengan pemberitahuan terlebih dahulu kepada Anggota SendMoney tersebut selama 3 (tiga) kali pemberitahuan selama 30 (tiga puluh) hari.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>c.	Sengketa berasal dari laporan Anggota SendMoney yang dilaporkan melalui support@SendMoney.com dan disertai dengan bukti-bukti yang kuat.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3.	SendMoney akan membekukan dana anggota SendMoney, apabila akun anggota SendMoney tersebut tidak aktif selama 6 (enam) bulan berturut – turut sesuai dengan Pasal Penutupan Akun poin 2 huruf a dan Anggota SendMoney tersebut tidak dapat dihubungi oleh pihak SendMoney dalam rangka pemberitahuan perihal pengembalian dana tersebut dengan pemberitahuan terlebih dahulu kepada anggota SendMoney tersebut selama 3 (tiga) kali pemberitahuan selama 30 (tiga puluh) hari.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>4.	Akun anggota SendMoney yang telah dinon-aktifkan dapat diaktifkan kembali dengan ketentuan sebagai berikut:</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>a.	Memberikan data diri dan data akun yang dibekukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>b.	Mengunggah identitas diri yang asli dan masih berlaku.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>c.	Memberikan dan/atau mengunggah data lainnya yang diperlukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>d.	Permohonan pengaktifan akun dikirimkan melalui email menggunakan email yang terdaftar dan ditujukan kepada support@SendMoney.com.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>e.	Riwayat transaksi pada akun SendMoney yang baru diaktifkan kembali tidak dapat dipulihkan kembali.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Kontak, Layanan Pelanggan, dan Pengetahuan Umum</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	SendMoney menyediakan layanan pelanggan SendMoney, kontak langsung dengan layanan pelanggan, dan Whatsapp resmi SendMoney untuk menjawab semua pertanyaan- pertanyaan dari anggota dan/atau calon anggota SendMoney di jam layanan SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2.	Anggota SendMoney dapat mengirimkan pertanyaan melalui form kontak kami di luar jam layanan SendMoney dan akan dibalas di jam layanan selanjutnya.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Pemalsuan Identitas/Identitas Tidak Sebenarnya</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1.	SendMoney mempunyai hak penuh untuk menyetujui dan menolak dalam proses verifikasi anggota dan jika diperlukan SendMoney juga berhak meminta data – data pendukung dalam proses verifikasi jika dirasa perlu.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2.	Apabila diketahui dan dapat dibuktikan bahwa calon dan/atau anggota SendMoney mengisi data diri palsu dan/atau menggunakan identitas palsu maka SendMoney berhak membekukan akun yang ada di dalamnya tanpa persetujuan terlebih dahulu.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3.	Akun yang dibekukan oleh pihak SendMoney seperti yang dijelaskan pada pasal Pemalsuan Identitas/Identitas Tidak Sebenarnya poin 2 di atas dapat diaktifkan kembali dengan syarat dan ketentuan sebagai berikut :</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>a.	Memberikan data diri dan data akun yang dibekukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>b.	Mengunggah identitas diri yang asli dan masih berlaku.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>c.	Memberikan dan/atau mengunggah data lainnya yang diperlukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml40]}>d.	Permohonan pengaktifan akun dikirimkan melalui email menggunakan email yang terdaftar dan ditujukan kepada support@SendMoney.com</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Pelepasan Tuntutan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>Jika Anggota SendMoney memiliki perselisihan dengan satu atau lebih anggota lainnya, Anggota SendMoney melepaskan SendMoney (termasuk induk perusahaan, direktur, dan karyawan) dari klaim dan tuntutan atas kerusakan dan kerugian (aktual dan tersirat) dari setiap jenis dan sifatnya, yang dikenal dan tidak dikenal, yang timbul dari atau dengan cara apapun berhubungan dengan sengketa tersebut. Dengan demikian, maka Anggota SendMoney dengan sadar dan tanpa tekanan membebaskan SendMoney dari segala macam tuntutan.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Ganti Rugi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>Anggota SendMoney akan membebaskan SendMoney (termasuk induk perusahaan, direktur, dan karyawan) dari tuntutan ganti rugi dan/atau biaya yang timbul dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anggota SendMoney melanggar Syarat dan Ketentuan ini, penggunaan layanan SendMoney yang tidak semestinya dan/atau pelanggaran Anggota SendMoney terhadap hukum atau hak-hak pihak ketiga.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Force Majeure</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>Anggota SendMoney akan membebaskan SendMoney dari segala macam tuntutan apapun dan dalam bentuk apapun, dalam hal SendMoney tidak dapat melaksanakan instruksi dari anggota (pengguna) baik sebagian maupun seluruhnya karena kejadian-kejadian atau sebab-sebab di luar kekuasaan atau kemampuan SendMoney, termasuk namun tidak terbatas pada segala gangguan virus komputer atau Trojan Horses atau komponen membahayakan yang dapat mengganggu layanan SendMoney, web browser atau sistem komputer SendMoney, Pengguna, Internet Service Provider, bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, sabotase, pemogokan masal, kebijakan pemerintah yang melarang PT Fasa Centra Artajaya memberikan layanan SendMoney, kegagalan sistem perbankan serta kejadian - kejadian atau sebab - sebab lain di luar kendali atau kemampuan SendMoney.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Penyelesaian Perselisihan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>Syarat dan Ketentuan ini diatur dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anggota SendMoney setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Syarat dan Ketentuan ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.</Text>

            <Text style={[mystyles.paragraph, mystyles.mb5, mystyles.bold]}>Penutup</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>Dengan mendaftar sebagai Anggota SendMoney, berarti Anda telah menyetujui syarat dan ketentuan ini dengan sadar dan tanpa paksaan. Syarat dan ketentuan ini dapat mengalami perubahan sewaktu – waktu tanpa pemberitahuan terlebih dahulu.</Text>
          </ScrollView>
        </View>
      </View>
    );
  }

}