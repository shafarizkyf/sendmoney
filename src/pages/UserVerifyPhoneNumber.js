import React from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput
} from '../components';
import mystyles from '../styles/mystyles';
import validate from 'validate.js';
import userVerifyPhoneNumber from '../validation/UserVerifyPhoneNumberValidation';
import languages from '../config/languages';

export default class UserVerifyPhoneNumber extends React.Component {

  state = {
    language: 'id',
    user: {},
    errors: []
  }

  async componentDidMount(){
    let user = await this.props.navigation.getParam('user');
    this.setState({language: this.props.screenProps.selectedLanguage, user});
  }

  verify(){
    let errors = validate(this.state, userVerifyPhoneNumber);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
    }
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.page.userPhone.form.phoneVerification[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <View style={[mystyles.form, { flex:1 }]}>
          <ScrollView>

            {
              this.state.sendOtp &&
              <Text style={[ mystyles.alertSuccess, mystyles.textGreen, mystyles.f12]}>
                { languages.page.userPhone.form.message[this.state.language] }
                <Text style={[mystyles.bold, mystyles.textGreen, mystyles.f12]}>
                  { ' ' + languages.page.userPhone.form.verificationCode[this.state.language] } 
                </Text>
              </Text>
            }

            <MyTextInput 
              label={ languages.page.userPhone.form.phoneNumber[this.state.language] }
              isRequiredMark={ true }
              value={ this.state.phone ? this.state.phone : this.state.user.phone }
              onChangeText={ (phone) => this.setState({phone}) }
              keyboardType={'numeric'}
              error={ this.state.errors.phone } />

            <Text style={[mystyles.labelSend]}>
              { languages.page.userPhone.form.send[this.state.language] }
            </Text>

            <MyTextInput 
              label={ languages.page.userPhone.form.verificationCode[this.state.language] }
              isRequiredMark={ true }
              value={ this.state.otp }
              onChangeText={ (otp) => this.setState({otp}) }
              keyboardType={'numeric'}
              error={ this.state.errors.otp } />

          </ScrollView>
        </View>

        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.verify() }>
          <Text style={ mystyles.formButton }>
            { languages.page.userPhone.form.validate[this.state.language] }
          </Text>
        </TouchableOpacity>        
      </View>
    );
  }
}