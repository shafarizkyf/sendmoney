import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Navbar } from '../components';
import mystyles from '../styles/mystyles';

export default class PrivacyPolicy extends React.Component {

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />
        <View style={[mystyles.navbarBorder]}></View>
        <View style={[mystyles.page]}>
          <ScrollView>
            <Text style={[mystyles.pageTitle, mystyles.mh20]}>Kebijakan Privasi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Kebijakan Privasi SendMoney ditetapkan oleh PT SendMoney (selanjutnya disebut “SendMoney”) sehubungan dengan pengumpulan, penggunaan dan penyingkapan informasi pribadi yang diperoleh dari para Anggota SendMoney (selanjutnya disebut “Anggota SendMoney”). Kebijakan Privasi ini berlaku untuk semua layanan yang tersedia pada aplikasi dan/atau situs SendMoney. Anggota SendMoney diharapkan dapat membaca kebijakan privasi ini dengan baik dan seksama sehingga Anggota SendMoney bisa memahami bagaimana SendMoney menggunakan data diri pribadi Anda sebagai Anggota SendMoney</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Dengan memberikan data diri dan/atau informasi pibadi Anda dalam mengakses SendMoney.com (selanjutnya disebut “situs SendMoney”) maka Anda telah memahami bagaimana data diri dan/atau informasi pribadi yang Anda berikan akan digunakan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>Dengan menggunakan layanan SendMoney, Anda setuju untuk terikat dengan Kebijakan Privasi (“Privacy Policy”) ini berikut Syarat dan Ketentuan SendMoney.</Text>

            <Text style={[mystyles.section]}>Identitas Pengguna</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Pada saat melakukan registrasi layanan SendMoney, Anggota SendMoney akan diminta untuk mengisi data secara lengkap dan benar (valid) mengenai, termasuk tidak terkecuali antara lain: nama lengkap, alamat, tanggal lahir, nomor telepon, nomor handphone, alamat email dan/atau alamat situs, nomor rekening bank, dan nama pemilik rekening bank, dan data lainnya yang dianggap perlu oleh SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Untuk proses verifikasi dan validasi data, Anggota SendMoney diharuskan untuk mengunggah scan kartu identitas yang masih berlaku berupa Kartu Tanda Penduduk (KTP), Surat Izin Mengemudi (SIM), atau PASSPORT. Untuk meningkatkan status keanggotaan, Anggota SendMoney juga diharuskan untuk mengunggah scan rekening tagihan berupa tagihan listrik, air, telepon, TV kabel, internet, gas, atau kartu kredit minimal tiga (3) bulan terakhir.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Anggota SendMoney yang berstatus Company atau Store diharuskan mengunggah surat domisili izin usaha atau domisili tinggal beserta legalitas perusahaan (jika ada) yang disahkan oleh pejabat setempat untuk proses verifikasi lebih lanjut.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Anggota SendMoney akan diminta SendMoney mengumpulkan informasi identifikasi dari Anggota SendMoney dengan berbagai cara, tidak terbatas pada, ketika Anggota SendMoney mengakses menu aplikasi dan/atau situs SendMoney, melakukan transaksi dan pembayaran, aktivitas, layanan, dan/atau fitur lainnya yang tersedia dalam aplikasi dan/atau situs SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Data diri Anggota SendMoney ini akan dikumpulkan oleh SendMoney sebagai data administrasi dan SendMoney dilarang atau tidak berhak untuk menyalahgunakan data diri tersebut untuk kepentingan komersial SendMoney dan/atau kepentingan lain yang dapat merugikan Anggota SendMoney.</Text>

            <Text style={[mystyles.section]}>Tujuan Pengumpulan Data</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Informasi data diri Anda yang diberikan kepada SendMoney berguna agar kami dapat memberikan layanan dan informasi yang dibutuhkan dengan lebih baik dan untuk itu, kami menggunakan informasi pribadi Anda untuk:</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1. Meningkatkan pelayanan pelanggan dimana informasi Anggota SendMoney membantu SendMoney untuk lebih efektif menanggapi permintaan layanan dan kebutuhan dukungan dari Anggota SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2. Menjawab pertanyaan dan/atau permintaan yang Anda ajukan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3. Mengirim pemberitahuan layanan dan memberikan dukungan pelanggan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>4. Mengantisipasi dan mengatasi permasalahan, mendeteksi dan mencegah kecurangan, terkait produk yang kami tawarkan.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>5. Memverifikasi identitas, audit, dan untuk tujuan kepatuhan terhadap standar peraturan pemerintah</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>6. Melakukan monitoring terhadap transaksi yang dilakukan dengan SendMoney dan/ataupun dengan merchant-merchant SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>7. Melakukan pencatatan segala informasi dari komputer dan browser Anda, termasuk alamat IP, cookie information, atribut perangkat lunak dan perangkat keras, dan halaman yang Anda minta.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20, mystyles.ml30]}>8. Mengelola atau melaksanakan kewajiban kami terkait dengan Perjanjian apapun antara Anda dengan SendMoney.</Text>

            <Text style={[mystyles.section]}>Cookie</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Cookie adalah berkas yang tersimpan di dalam komputer, termasuk smartphone, tablet, atau perangkat yang Anda gunakan. Berkas tersebut digunakan untuk memuat data-data sederhana tentang kebiasaan Anda saat menggunakan aplikasi browser. Cookie membantu browser Anda dalam menelusuri situs web dan cookie itu sendiri tidak akan mengumpulkan informasi apapun yang tersimpan pada komputer Anda. Cookie berfungsi untuk mengingat keterangan penting yang akan membantu dalam penggunaan situs, dalam hal pengakses memutuskan untuk mengunjungi kembali situs tersebut di kemudian hari.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>SendMoney menggunakan cookie untuk session login, menyimpan token CSFR, dan visitor tracking pada live chat Anggota SendMoney dalam menggunakan Layanan SendMoney. Anda bisa memilih untuk mengatur komputer, smartphone, tablet, atau perangkat lainnya untuk menolak cookie atau memberikan peringatan ketika cookie dikirimkan.</Text>

            <Text style={[mystyles.section]}>Kerahasiaan dan Keamanan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>SendMoney menerapkan teknologi dan kebijakan pengumpulan data, penyimpanan, praktek pengolahan yang tepat, dan langkah-langkah keamanan sesuai dengan aturan yang berlaku di Indonesia dengan tujuan melindungi privasi Anda terhadap akses yang tidak sah, penggunaan yang tidak patut, informasi transaksi, dan data yang tersimpan di SendMoney. Pertukaran data sensitif dan pribadi antara host SendMoney dan Anggota SendMoney terjadi melalui jalur aman SSL dan dienkripsi dan dilindungi dengan tanda tangan digital. Kami akan memperbarui langkah- langkah ini seiring perkembangan teknologi baru sesuai kebutuhan.</Text>

            <Text style={[mystyles.section]}>Perubahan Data</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>SendMoney memberlakukan kebijakan verifikasi data diri oleh admin sebelum data diri diubah. Pihak SendMoney berhak menerima atau menolak perubahan data diri yang diajukan tergantung seberapa besar perubahannya dan SendMoney berhak meminta bukti perubahan yang diajukan, sepanjang bukti data tersebut benar dan sesuai maka data diri Anggota SendMoney dapat diubah.</Text>

            <Text style={[mystyles.section]}>Newsletter</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>Kami berhak untuk mengirimkan Anda informasi/komunikasi terkait dengan jasa layanan SendMoney, seperti jasa pengumuman, pesan administrasi dan surat kabar SendMoney yang dinilai sebagai bagian dari pemasaran produk SendMoney.</Text>

            <Text style={[mystyles.section]}>Pengungkapan Informasi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Informasi data diri yang didapat SendMoney dari Anggota SendMoney akan disimpan untuk jangka waktu tertentu. SendMoney tidak akan menjual, menukar, atau mengirimkan informasi yang diberikan oleh Anggota SendMoney kepada pihak ketiga yang tidak berkepentingan. SendMoney dapat menyampaikan informasi yang didapat dari Anggota SendMoney, termasuk namun tidak terbatas pada, antara lain:</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>1. Jika diperintahkan oleh hukum dan/atau permintaan dari pihak berwajib, bank sentral, maupun lembaga keuangan lainnya.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>2. Memenuhi kepatuhan pada Undang-Undang atau proses pengadilan atau hukum.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10, mystyles.ml30]}>3. Melindungi terhadap penyalahgunaan atau penggunaan tanpa izin dari aplikasi menu SendMoney.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>Anggota SendMoney dengan ini membebaskan dan melepaskan SendMoney dari segala tuntutan, klaim, ganti rugi, biaya perkara, dan/atau gugatan berkaitan dengan pemberian akses atas informasi dimaksud.</Text>

            <Text style={[mystyles.section]}>Perubahan Terhadap Kebijakan dan Privasi</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>SendMoney berhak dan dapat mengubah serta memperbaharui Kebijakan Privasi ini kapan saja tanpa pemberitahuan sebelumnya. Apabila kami mengganti atau memperbaharui kebijakan privasi kami, kami akan memberitahu perubahan kebijakan tersebut melalui email dan/atau dalam situs SendMoney dan diharapkan Anda dapat terus mengupdate Kebijakan Privasi ini.</Text>

            <Text style={[mystyles.section]}>Penutup</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>Dengan menggunakan layanan SendMoney, berarti Anda menyetujui Kebijakan Privasi ini. Jika Anda tidak setuju dengan kebijakan ini, Anda dapat memilih untuk tidak menggunakan layanan SendMoney. Anda disarankan untuk selalu membaca terlebih dahulu Kebijakan Privasi serta Syarat dan Ketentuan SendMoney sebelum menggunakan layanan yang terdapat pada situs dan/atau aplikasi SendMoney.</Text>
          </ScrollView>
        </View>
      </View>
    );
  }

}