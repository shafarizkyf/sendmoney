import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import {
  Navbar,
  TopMenu, 
  Footer 
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';

export default class Home extends Component {

  state = {
    language: 'id'
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />
        <TopMenu 
          homePage={ () => this.props.navigation.navigate('home') }
          servicePage={ () => this.props.navigation.navigate('service') } 
          featurePage={ () => this.props.navigation.navigate('feature') }
          pricingPage={ () => this.props.navigation.navigate('pricing') }
          language={ this.state.language } selected={ 2 } />
        
        <View style={[mystyles.page]}>
          <ScrollView>
            <Text style={[mystyles.pageTitle, mystyles.mh20]}>{ language.home.navbarMenu.service[this.state.language] }</Text>
            <Text style={[mystyles.section]}>Konversi Mata Uang (IDR-USD-EUR)</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>Salah Satu Layanan Unggulan Dari SendMoney Adalah Anda Dapat Melakukan Konversi Mata Uang Dari Rupiah Ke Dollar, Rupiah Ke Euro, Dollar Ke Rupiah, Euro Ke Rupiah, Dollar Ke Euro, Ataupun Euro Ke Dollar.</Text>
            <Text style={[mystyles.paragraph]}>Langkah-Langkah Cara Konversi Mata Uang :</Text>
            <Text style={[mystyles.paragraph]}>1.Tentukan Mata Uang Dasar Dan Konversi Mata Uang Tujuan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>2.Klik Oke Jika Sudah Setuju Dengan Nilai Konversi Mata Uang</Text>

            <Text style={[mystyles.section]}>Deposit</Text>
            <Text style={[mystyles.paragraph]}>a.Deposit Otomatis</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Tentukan Mata Uang Yang Digunakan Untuk Melakukan Pengisian Deposit (Mata Uang Dalam Rupiah, Dollar & Euro)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Limit Deposit Dalam Saldo Rupiah Akan Dibatasi Sesuai Dengan Jenis Status Keanggotaan di SendMoney. Untuk Status User Active (Max. Deposit IDR 50.000.000) dan Untuk User Verified (Max. Deposit IDR 10.000.000.000)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Jika Mata Uang Dipilih Adalah Rupiah Maka Metode Deposit Otomatis Yang Muncul : BCA Virtual Account, Tetapi Jika Mata Uang Yang Dipilih Adalah Dollar Atau Euro Maka Metode Deposit Otomatis Yang Muncul Adalah Perfect Money</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Masukkan Jumlah Nominal Yang Akan Didepositkan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Masukkan Keterangan Deposit</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb10]}>-Setelah Anda Klik Proses Maka Akan Muncul Informasi Tata Cara Transfer Otomatis di ATM, Mobile Banking, Internet Banking Ataupun Tata Cara Pembayaran Otomatis Metode Lain.</Text>
            <Text style={[mystyles.paragraph]}>b.Deposit Manual</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Limit Deposit Dalam Saldo Rupiah Akan Dibatasi Sesuai Dengan Jenis Status Keanggotaan di SendMoney. Untuk Status User Active (Max. Deposit IDR 50.000.000) dan Untuk User Verified (Max. Deposit IDR 10.000.000.000)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Tentukan Mata Uang Yang Digunakan Untuk Melakukan Pengisian Deposit (Mata Uang Dalam Rupiah, Dollar, & Euro)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Tentukan Nama Bank Anda - Nomor Rekening</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Tentukan Jumlah Yang Akan Didepositkan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Isi Keterangan Deposit</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-Setelah Itu Akan Muncul Bank Tujuan Transfer Beserta Keterangan Yang Harus Disertakan Sebelum Melakukan Transfer di ATM, Mobile Banking Atau Internet Banking.</Text>

            <Text style={[mystyles.section]}>Kirim Dana</Text>
            <Text style={[mystyles.paragraph]}>Metode Kirim Dana Pada SendMoney Terbagi Atas 2 Yaitu :</Text>
            <Text style={[mystyles.paragraph]}>A.Transfer Ke Sesama SendMoney</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Dari" Otomatis Akan Terisi Dengan ID SendMoney Anda Yang Telah Terdaftar</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Ke User SendMoney" Merupakah Kolom Isian Tujuan Transfer Dana Anda Akan Berisi Id SendMoney</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Atas Nama" Otomatis Akan Terisi Setelah Memasukkan Id SendMoney Tujuan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Mata Uang" Merupakan Pilihan Mata Uang Yang Akan Digunakan Untuk Transaksi Saat Pengiriman Dana</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Jumlah" Merupakan Angka Nominal Dana Yang Akan Dikirimkan Disesuaikan Dengan Mata Uang Yang Digunakan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Keterangan" Merupakan Informasi Tambahan Yang Dapat Dilihat Oleh User Penerima Dana</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb10]}>-Setelah Kolom Isian Terisi Semua Maka Klik Proses Untuk Melanjutkan</Text>
            <Text style={[mystyles.paragraph]}>B.Transfer Ke Bank Tujuan Transfer (Lokal & Internasional Bank)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Dari" Otomatis Akan Terisi Dengan ID SendMoney Anda Yang Telah Terdaftar</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Bank Transfer" Merupakan Bank Tujuan Untuk Pengiriman Dana (Berisi Bank Lokal dan Internasional Tujuan Transfer)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Nomor Rekening" Merupakan ID Rekening Tujuan Disesuaikan Dengan Bank Tujuan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Atas Nama" Merupakan Nama Penerima Dana Melalui Metode Transfer Bank</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Mata Uang" Merupakan Mata Uang Yang Digunakan Untuk Mentransfer Dana (Harap Sesuaikan Dengan Negara Penerima Transfer)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Jumlah" Merupakan Angka Nominal Dana Yang Akan Dikirimkan Disesuaikan Dengan Mata Uang Yang Digunakan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Keterangan" Merupakan Informasi Tambahan Yang Dapat Dilihat Oleh Penerima Dana</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb20]}>-Setelah Kolom Isian Terisi Semua Maka Klik Proses Untuk Melanjutkan</Text>

            <Text style={[mystyles.section]}>Penarikan</Text>
            <Text style={[mystyles.paragraph]}>Layanan Ini Digunakan Untuk Mencairkan Dana di SendMoney Ke Account Bank Sendiri</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Mata Uang" Disesuaikan Dengan Bank Pencairan Anda</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Bank Anda" Disesuaikan Dengan Bank Tujuan Anda Mencairkan Dana (Account Bank Harus Merupakan Atas Nama Anda Sendiri)</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb5]}>-"Jumlah" Merupakan Angka Nominal Dana Yang Akan Dicairkan Disesuaikan Dengan Mata Uang Yang Digunakan</Text>
            <Text style={[mystyles.paragraph, mystyles.ml30, mystyles.mb20]}>-"Keterangan" Merupakan Informasi Tambahan Yang Akan Muncul Di Mutasi Transaksi Bank Anda</Text>

          </ScrollView>          
          <Footer />
        </View>
      </View>
    )
  }
}