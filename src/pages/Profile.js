import React, { Component } from 'react';
import {
  View, 
  ScrollView, 
  Text, 
  Image, 
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import { ImagePicker } from 'expo';
import {
  Navbar,
  Breadcrumb,
} from '../components';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import languages from '../config/languages';
import Axios from 'axios';
import api from '../config/api';


export default class Profile extends Component{

  state = {
    language: 'id',
    image: null,
    user: {}
  }
  
  componentDidMount(){
    this.setState({language:this.props.screenProps.selectedLanguage});
    this.userData();
  }

  userData(){

    const data = {
      com: api.com.getUser,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    }

    Axios.post(api.baseUrl, data)
    .then(response => {
      response = response.data.data[0];
      console.log('response get user -->', response);

      let user = {
        id: response.id,
        name: response.namaLengkap,
        birthday: response.tanggalLahir,
        birthPlace: response.tempatLahir,
        email: response.email,
        phone: response.noHandphone,
        address: response.alamat,
        country: response.negara,
        province: response.provinsi,
        city: response.kota,
        zipcode: response.kodePos,
        userStatus: response.statusUser,
        identityStatus: response.statusIdentitas,
        identityImage: response.tipeIdentitas,
        validity: response.berlakuHingga,
        note: response.note,
        identityType: response.tipeIdentitas,
        identityNumber: response.nomorIdentitas,
        userImage: '',
      }

      this.setState({user});

    }).catch(error => {
      console.log('error get user -->', error);
    });

  }

  async pickDocument(){
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1],
      base64: true
    });

    console.log('selected image -->', result)

    if (!result.cancelled) {
      this.setState({ image: {uri: result.uri, base64:`data:image/jpg;base64,${result.base64}`} });
    }
  }

  render(){

    function DataRow(props){
      return (
        <View style={[styles.dataRow, props.grey ? mystyles.bgGrey : {}]}>
          <Text style={[mystyles.bold, mystyles.textDarkGrey]}>{props.field}</Text>
          <Text style={[]}>{props.value}</Text>
        </View>
      );
    }

    function DataHeader(props){
      return(
        <View style={[mystyles.row, mystyles.mt10]}>
          <Text style={[ styles.section, styles.w80, mystyles.mr5, mystyles.f14, mystyles.bold ]}>{ props.section }</Text>
          <View style={[mystyles.row, styles.iconBtn, styles.bgGreen, styles.w20]}>
            <Image source={ require('../../assets/img/ubah.png') } style={[mystyles.icon20]} />
            <TouchableOpacity activeOpacity={0.8} onPress={ props.editPage } >
              <Text style={[mystyles.textWhite, mystyles.ml5]}>Ubah</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }

    return(
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.menu.profile[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>
        
        <ScrollView>
          <View>
            <View style={[mystyles.mh20]}>
              <View style={[mystyles.row, mystyles.mt20]}>
                <TouchableOpacity onPress={()=>this.pickDocument()}>
                  <Image 
                    source={ this.state.image ? {uri: this.state.image.base64} : require('../../assets/img/avatar.png') } 
                    style={[styles.avatar, !this.state.image && {tintColor:colors.navy}]} 
                  />
                </TouchableOpacity>
                <View style={[mystyles.column, mystyles.ml20]}>
                  <Text style={[mystyles.f18, mystyles.bold, mystyles.mb10]}>{this.state.user.name}</Text>
                  <Text style={[mystyles.f14, mystyles.mb5]}>
                    { languages.page.profile.view.status[this.state.language] }: 
                    <Text style={[styles.textGreen, mystyles.bold]}>{this.state.user.userStatus}</Text>
                  </Text>
                  <View style={[mystyles.row]}>
                    <Image source={ require('../../assets/img/hp.png') } style={ styles.phone } />
                    <Text style={[mystyles.ml5]}>{this.state.user.phone}</Text>
                  </View>
                </View>
              </View>
            </View>
            
            <DataHeader 
              section={ languages.page.profile.view.userData[this.state.language] } 
              editPage={ () => this.props.navigation.navigate('biodata', {
                breadcrumb: languages.page.profile.view.userData[this.state.language],
                updateData: true,
                user: this.state.user
              })} 
            />

            <DataRow
              field={ languages.page.profile.view.sendMoneyAccount[this.state.language] } 
              value={ this.state.user.id } 
              grey={ true } 
            />

            <DataRow
              field={ languages.page.profile.view.name[this.state.language] } 
              value={ this.state.user.name } 
            />

            <DataRow
              field={ languages.page.profile.view.birthday[this.state.language] } 
              value={ this.state.user.birthday } 
              grey={ true }
            />
            
            <DataRow 
              field={ languages.page.profile.view.email[this.state.language] }
              value={ this.state.user.email } 
            />

            <DataRow 
              field={ languages.page.profile.view.phone[this.state.language] } 
              value={ this.state.user.phone } 
              grey={ true }
            />

            <TouchableOpacity onPress={() => this.props.navigation.navigate('verifyPhoneNumber', {user:this.state.user}) }>
              <Text style={[styles.labelRed]}>
                { languages.page.profile.view.verifyPhoneNumber[this.state.language] }
              </Text>
            </TouchableOpacity>

            <DataHeader 
              section={ languages.page.profile.view.addressData[this.state.language] }
              editPage={ () => this.props.navigation.navigate('address', {user:this.state.user}) } 
            />

            <DataRow 
              field={ languages.page.profile.view.address[this.state.language] }
              value={ this.state.user.address } 
              grey={ true } 
            />

            <DataRow
              field={ languages.page.profile.view.city[this.state.language] } 
              value={ this.state.user.city } 
            />
            
            <DataRow
              field={ languages.page.profile.view.province[this.state.language] }
              value={ this.state.user.province }
              grey={ true }
            />

            <DataRow
              field={ languages.page.profile.view.zipcode[this.state.language] }
              value={ this.state.user.zipcode }
            />
            
            <DataRow
              field={ languages.page.profile.view.country[this.state.language] }
              value={ this.state.user.country }
              grey={ true }
            />

            <DataHeader 
              section={ languages.page.profile.view.userDocument[this.state.language] }
              editPage={ () => this.props.navigation.navigate('verifyIdentity', {user:this.state.user}) } 
            />
            
            <DataRow 
              field={ languages.page.profile.view.identityType[this.state.language] } 
              value={ this.state.user.identityType } 
              grey={ true }
            />
            
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  avatar:{
    width: 100,
    height: 100,
    // tintColor: colors.navy
  },
  w80:{
    width:'70%'
  },
  w20:{
    width:'30%'
  },
  textGreen:{
    color: '#63dd16',
  },
  bgGreen:{
    backgroundColor: '#63dd16'
  },
  phone:{
    width: 10.4,
    height: 20
  },
  iconBtn:{
    padding:10,
  },
  section:{
    backgroundColor: colors.yellow,
    paddingHorizontal: 20,
    paddingVertical: 10,
    color: colors.brown
  },
  dataRow:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingHorizontal:20,
    paddingVertical: 5,
  },
  labelRed:{
    fontSize: 10,
    color: 'white',
    backgroundColor: colors.red,
    padding: 3,
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 5,
    marginRight: 20,
    borderRadius: 5,
  }
})