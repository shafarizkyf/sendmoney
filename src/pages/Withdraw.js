import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Navbar,
  Breadcrumb,
  Balance,
  Tabs,
  Label
} from '../components';
import mystyles from '../styles/mystyles';
import { WithdrawDefault, WithdrawRiwayat } from '.';
import validate from 'validate.js';
import withdrawValidation from '../validation/WithdrawValidation';
import Axios from 'axios';
import api from '../config/api';
import languages from '../config/languages';

export default class Withdraw extends Component {
  
  state = {
    language: 'id',
    tab: 1,
    tab1: [],
    tab2: [],
    tabs: [
      {label: languages.page.withdraw.tab.withdraw[this.props.screenProps.selectedLanguage], tabId: 1},
      {label: languages.page.withdraw.tab.history[this.props.screenProps.selectedLanguage], tabId: 2 },
    ],
    errors: [],
    history: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    this.banks();
    this.currency();
  }

  async banks(){
    if(this.props.screenProps.banks){
      this.setState({banks: [{value: '', label:languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.banks]});
    }else{
      await this.props.screenProps.getBanks();
      this.setState({banks: [{value: '', label:languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.banks]});
    }
  }
  
  async currency(){
    if(this.props.screenProps.currency){
      this.setState({currency: [{value: '', label:languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }else{
      await this.props.screenProps.getCurrency();
      this.setState({currency: [{value: '', label:languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }
  }

  async onProses(obj) {
    let withdrawRules = {};
    if(this.state.tab === 1){
      withdrawRules = withdrawValidation;
      await this.setState({tab1: obj});
    }

    let errors = validate(obj, withdrawRules);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
    }

    if(!errors && this.state.tab === 1){
      this.proceedWithdraw();
    }
  }

  proceedWithdraw(){
    let data = {
      com: api.com.withdraw,
      kodeBank: this.state.tab1.bank,
      nomorRekening: this.state.tab1.number,
      mataUang: this.state.tab1.currency,
      jumlah: this.state.tab1.amount,
      keterangan: this.state.tab1.note,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    console.log('data withdraw -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('withdraw -->', response);

      if(response.resultCode == api.response.success){
        this.setState({success: languages.response.transaction.success[this.state.language]});

      }else if(response.resultCode == api.response.error){
        this.setState({error: languages.response.transaction.error[this.state.language]});
      }

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error withdraw -->', error);
    });
  }

  render(){

    function TabContent(props) {
      if (props.tab == 1) {
        return <WithdrawDefault
        language={props.language}
        banks={props.banks}
        currency={props.currency}
        onProses={(obj) => props.onProses(obj)} 
        errors={props.errors} values={props.tab1} />

      } else if (props.tab == 2) {
        return <WithdrawRiwayat
        language={props.language}
        onProses={(obj) => props.onProses(obj)}
        history={props.history} />
      }
    }
    
    return(
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>

        <Breadcrumb 
          title={ languages.menu.withdraw[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <Balance data={ this.props.screenProps.balance } />

        <Tabs 
          tabs={this.state.tabs} 
          selected={this.state.tab} 
          navigation={(tab) => { this.setState({ tab }) }} />
          
        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <TabContent
          language={ this.state.language }
          banks={ this.state.banks }
          currency={ this.state.currency }
          onProses={ (obj) => this.onProses(obj) }
          errors={ this.state.errors }
          tab={ this.state.tab } 
          tab1={ this.state.tab1 }
          tab2={ this.state.tab2 }
          history={ this.state.history } />
      </View>
    );
  }

}