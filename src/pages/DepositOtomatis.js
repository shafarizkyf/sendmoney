import React, { Component } from 'react';
import { View, Text, ScrollView, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import mystyles from '../styles/mystyles';
import { CurrencyOption, MyTextInput, MySelectInput } from '../components';
import flag from '../config/flag';
import languages from '../config/languages';

export default class DepositOtomatis extends Component {

  state = {
    banks: [{value: '', label:languages.blankOptions.bank[this.props.language]}],
    currencyList: [{value: '', label:languages.blankOptions.currency[this.props.language]}]
  };

  componentDidMount(){    
    if(this.props.banks){
      this.setState({banks: this.props.banks});
    }

    if(this.props.currency){
      this.setState({currencyList: this.props.currency});
    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20, mystyles.mt10]}>

            <CurrencyOption 
              onChange={(currency, position) => { this.setState({currency, flag: this.state.currencyList[position].flag})}} 
              value={ this.state.currency }
              flag={ this.state.flag }
              error={ this.props.errors.currency }
              items={ this.state.currencyList }
              text={ languages.page.deposit.form.depositOtomatis.currency[this.props.language] } />

            <MySelectInput 
              label={ languages.page.deposit.form.depositOtomatis.bank[this.props.language] }
              items={this.state.banks}
              isRequiredMark={true}
              onChangeSelect={ (val) => { this.setState({bank:val}) } }
              value={ this.state.bank }
              error={ this.props.errors.bank } />
      
            <MyTextInput 
              label={ languages.page.deposit.form.depositOtomatis.amount[this.props.language] }
              isRequiredMark={true}
              value={this.state.amount}
              onChangeText={ (val) => { this.setState({amount:val}) } }
              keyboardType="numeric" 
              error={ this.props.errors.amount } />

            <MyTextInput 
              label={ languages.page.deposit.form.depositOtomatis.note[this.props.language] } 
              isRequiredMark={true}
              value={this.state.note}
              onChangeText={ (val) => { this.setState({note:val}) } } 
              error={ this.props.errors.note } />
      
          </View>
        </ScrollView>
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.onProses(this.state) }>
          <Text style={mystyles.formButton}>
            { languages.page.deposit.form.depositOtomatis.proceed[this.props.language] }
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
  
}