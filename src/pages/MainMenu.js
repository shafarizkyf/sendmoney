import React, { Component } from 'react';
import { 
  View, 
  ImageBackground,
  ScrollView
} from 'react-native';
import {
  Navbar,
  TopMenu,
  Balance,
  MenuButton
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';

export default class MainMenu extends Component{
  
  state = {
    language: 'id'
  }

  componentDidMount(){
    console.log('screenProps -->', this.props.screenProps);
    this.setState({language: this.props.screenProps.selectedLanguage});

    if(this.props.screenProps.user){
      this.props.screenProps.getUserBanks();
      this.props.screenProps.getUserCurrency();
    }
  }

  render(){
    return(
      <View style={ mystyles.page }>

        <Navbar 
          openDrawer={()=>this.props.navigation.openDrawer()}
          notification={()=>this.props.navigation.navigate('notification') } />

        <TopMenu 
          homePage={ () => { this.props.navigation.navigate('home') } }
          featurePage={ () => { this.props.navigation.navigate('feature') } }
          pricingPage={ () => { this.props.navigation.navigate('pricing') } }
          language={ this.state.language } selected={ 0 } 
        />

        <Balance data={ this.props.screenProps.balance } />
        
        <View style={[mystyles.page]}>
          <ImageBackground source={ require('../../assets/img/background.png') } style={ mystyles.loginBg } />
          <ScrollView style={[ mystyles.mt10 ]}>
            <View style={[mystyles.mh20]}>
              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('exchange') } }
                label={ language.menu.exchange[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('deposit') } }
                label={ language.menu.deposit[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('send') } }
                label={ language.menu.transfer[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('withdraw') } }
                label={ language.menu.withdraw[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('mutation') } }
                label={ language.menu.mutation[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('profile') } }
                label={ language.menu.profile[this.state.language] } />

              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('setting') } }
                label={ language.menu.setting[this.state.language] } />
 
              <MenuButton 
                navigation={ () => { this.props.navigation.navigate('verifyIdentity') } }
                label={ language.menu.verification[this.state.language] } />
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }
}