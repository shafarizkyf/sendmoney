import React, { Component } from 'react';
import { 
  View, 
  Text,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import {
  Navbar,
  TopMenu,
  Label,
} from '../components';
import language from '../config/languages';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';
import Axios from 'axios';
import api from '../config/api';
import validate from 'validate.js';
import loginValidation from '../validation/LoginValidation';

export default class ForgetPassword extends Component {

  state = {
    language: 'id',
    isRecoveryCodeSent: false,
    errors: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  sendRecoveryCode(){

  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar
          showNotificationIcon={false}
          openDrawer={ () => this.props.navigation.openDrawer() } />

        <TopMenu 
          homePage={ () => { this.props.navigation.navigate('home') } }
          featurePage={ () => { this.props.navigation.navigate('feature') } }
          pricingPage={ () => { this.props.navigation.navigate('pricing') } }
          language={ this.state.language } selected={ 0 } />

        <View style={ mystyles.page }>
          <ImageBackground source={ require('../../assets/img/background.png') } style={ mystyles.loginBg } />
          
          { this.state.error && <Label isError={true} message={this.state.error} /> }

          <View style={ styles.loginForm }>
            <Text style={ styles.title }>
            { language.page.forgetPassword.title[this.state.language] }
            </Text>
            
            <TextInput 
              style={ styles.textInput }
              underlineColorAndroid={ 'rgba(0,0,0,0)' } 
              onChangeText={ (email) => this.setState({email}) }
              value={ this.state.email }
              placeholder="E-mail"
              autoCapitalize="none" 
            />

            {
              this.state.errors.email &&
              <Text style={[mystyles.f12, mystyles.textRed, mystyles.mb10]}>{this.state.errors.email}</Text>
            }

            <TextInput 
              style={ styles.textInput }
              underlineColorAndroid={ 'rgba(0,0,0,0)' }
              onChangeText={ (code) => this.setState({code}) }
              value={ this.state.code }
              placeholder="Kode Verifikasi" 
            />
  
            {
              this.state.errors.code &&
              <Text style={[mystyles.f12, mystyles.textRed, mystyles.mb10]}>{this.state.errors.code}</Text>
            }

            <TouchableOpacity onPress={ () =>  this.sendRecoveryCode() }>
              <Text style={ styles.loginBtn } >
                { language.page.forgetPassword.sendCodeBtn[this.state.language] }
              </Text>
            </TouchableOpacity>

          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loginForm:{
    marginTop: 50,
    marginHorizontal: 40,
    paddingHorizontal: 30,
    paddingVertical: 30,
    borderRadius: 10,
    backgroundColor: 'rgba(51, 111, 161, 0.9)',
    elevation: 10,
  },
  title:{
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
  },
  textInput:{
    paddingVertical:5,
    paddingHorizontal:10,
    marginBottom: 10,
    borderRadius: 3,
    color: colors.black,
    backgroundColor: '#cfd8de',
  },
  loginBtn:{
    padding: 10,
    backgroundColor: colors.orange,
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    borderRadius: 3,
  }
});