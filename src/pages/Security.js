import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { 
  Navbar, 
  Breadcrumb,
  Label
} from '../components';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import Axios from 'axios';
import api from '../config/api';
import languages from '../config/languages';

export default class Security extends Component{

  state = {
    language: 'id',
    toggle: this.props.screenProps.user.hasPin
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
  }

  addSecurity(){
    let data = {
      com: api.com.addSecurity,
      setPinAktif: !this.state.toggle,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    this.setState({toggle: !this.state.toggle});
    console.log('data set security -->', data);

    Axios.post(api.baseUrl, data)
    .then(response => {
      response = response.data;
      console.log('set security -->', response);

      if(response.resultCode == api.response.success){
        this.setState({
          toggle: !this.state.toggle, 
          success: !this.state.toggle === true 
            ? languages.page.security.response.activate[this.state.language]
            : languages.page.security.response.disabled[this.state.language]
        });
      }

    }).catch(error => {
      this.setState({error: 'Internal Server Error'});
      console.log('error exchange -->', error);
    });
  }

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') } />
          
        <Breadcrumb 
          title={ languages.menu.securiy[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />
        
        { this.state.success && <Label message={this.state.success} /> }
        { this.state.error && <Label isError={true} message={this.state.error} /> }

        <View style={[styles.borderBottom]}>
          <View style={[mystyles.card]}>
            <View style={[mystyles.row, mystyles.spaceBetween]}>
              <View style={[mystyles.column]}>
                <Text style={[mystyles.bold, mystyles.f12]}>
                  { languages.page.security.text.activate[this.state.language] }
                </Text>
                <Text style={[mystyles.f12, mystyles.textDarkGrey]}>
                  { languages.page.security.text.addSecurityLayer[this.state.language] }
                </Text>
              </View>
              <TouchableOpacity onPress={() => this.addSecurity() }>
                <Image 
                  source={ this.state.toggle ? require('../../assets/img/active.png') : require('../../assets/img/unactive.png') } 
                  style={[styles.toggle]} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  borderBottom:{
    borderBottomColor: colors.grey,
    borderBottomWidth: 1
  },
  toggle:{
    width: 36.6,
    height: 20,
  }
});