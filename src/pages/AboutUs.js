import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import { Navbar } from '../components';
import mystyles from '../styles/mystyles';

export default class AboutUs extends React.Component {

  render(){
    return(
      <View style={[mystyles.page]}>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />
        <View style={[mystyles.navbarBorder]}></View>
        <View style={[mystyles.page]}>
          <ScrollView>
            <Text style={[mystyles.pageTitle, mystyles.mh20]}>Tentang Kami</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>SendMoney Adalah Produk Yang Dikeluarkan Oleh PT SendMoney Yang Bergerak di Bidang Jasa Keuangan Untuk Transfer Ke Bank Lokal Maupun Internasional.</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>SendMoney Hadir Untuk Menjawab Kebutuhan Transfer Uang Ke Bank Lokal Maupun Internasional Dalam Hitungan Menit. Kami Hadir Dikarenakan Kami Melihat Bahwa Kecepatan Mengirim Uang Ke Bank Internasional Saat Ini Lambat Karena Butuh Waktu Berhari-Hari Ataupun Sebaliknya Dari Luar Negeri Ke Bank Lokal Juga Butuh Waktu Berhari-Hari. Dari Pengalaman Itu Kami Membuat Sistem Pengiriman Uang Dalam Hitungan Menit Dengan Metode Yang Lebih Cepat.</Text>

            <Text style={[mystyles.section]}>Visi Perusahaan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb20]}>Menjadi Perusahaan Jasa Keuangan Yang Memiliki Kompetensi, Kualitas, dan Siap Menjawab Tantangan Masa Depan Dalam Teknologi Keuangan Baik Secara Nasional & Global</Text>

            <Text style={[mystyles.section]}>Misi Perusahaan</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>- Mewujudkan Sistem Teknologi Jasa Keuangan Yang Selalu Terbarukan Menyesuaikan Dengan Kebutuhan Teknologi Jasa Keuangan Terbaru</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>- Meningkatkan Kinerja Pelayanan Serta Menjalankan Standard Procedure Untuk Membantu Semua Pengguna Agar Dapat Menggunakan Layanan Dengan Baik</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>- Meningkatkan, Memperbaharui Serta Selalu Memelihara Keamanan Sistem Teknologi Jasa Keuangan Untuk Mendukung Transaksi Lokal Maupun Internasional Pengguna</Text>
            <Text style={[mystyles.paragraph, mystyles.mb10]}>- Mewujudkan Sistem Keamanan Jasa Keuangan Yang Aman, Efisien dan Lancar</Text>
          </ScrollView>
        </View>
      </View>
    );
  }

}