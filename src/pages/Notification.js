import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import {
  Navbar,
  Breadcrumb,
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';


export default class Notification extends Component{

  render(){
    function Card (props){
      return (
        <View style={[mystyles.card]}>
          <View style={[mystyles.row, mystyles.spaceBetween]}>
            <Text style={[mystyles.f14]}>{ props.type.toUpperCase() }</Text>
            <Text style={[mystyles.f10, mystyles.selfCenter, mystyles.textNavi]}>25 Oktober 2018 - 10:59:59</Text>
          </View>
          <Text style={[mystyles.f10, mystyles.mt10, mystyles.textDarkGrey]}>Deposit dari BRI a/n JUNAIDI sebesar Rp100.000 Berhasil</Text>
        </View>
      );
    }
  
    return(
      <View style={[mystyles.page, mystyles.bgGrey]}>
        <Navbar openDrawer={()=>this.props.navigation.openDrawer()} />
        <Breadcrumb 
          title={ "Notifikasi" } 
          onBack={ () => { this.props.navigation.goBack() } } />
          
        <ScrollView>
          <View style={[]}>
            <View style={[mystyles.mh20]}>
              {/* <View style={[mystyles.mt10]}></View>
              <Card type="deposit" />
              <Card type="penarikan" />
              <Card type="dana masuk" />
              <Card type="penarikan" />
              <Card type="penarikan" />
              <Card type="dana masuk" />
              <Card type="dana masuk" />
              <Card type="deposit" />
              <Card type="deposit" /> */}
            </View>            
          </View>
        </ScrollView>
      </View>
    );
  }

}