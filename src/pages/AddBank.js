import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView, 
  TouchableOpacity 
} from 'react-native';
import { 
  Navbar, 
  Breadcrumb, 
  CurrencyOption, 
  MyTextInput, 
  MySelectInput, 
  Label 
} from '../components';
import mystyles from '../styles/mystyles';
import flag from '../config/flag';
import validate from 'validate.js';
import addBankValidation from '../validation/AddBankValidation';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';

export default class AddBank extends Component{
  
  state = {
    language: 'id',
    banks: [],
    currencyList: [],
    errors: []
  };

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});

    if(this.props.screenProps.banks){
      this.setState({banks: this.props.screenProps.banks});
    }

    if(this.props.screenProps.currency){
      this.setState({currencyList: this.props.screenProps.currency});
    }
  }

  save(){
    let errors = validate(this.state, addBankValidation);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      let data = {
        com: api.com.addUserBank,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password,
        kodeBank: this.state.bank,
        mataUang: this.state.currency,
        nomorRekening: this.state.number,
        atasNama: this.state.name,
        status: true
      };

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        console.log('add banks -->', response);
        if(response.resultCode == api.response.success){
          this.setState({success: languages.response.saving.success[this.state.language]});
        
        }else if(response.resultCode == api.response.error){
          this.setState({error: languages.response.saving.error[this.state.language]});
        }

      }).catch(error => {
        this.setState({error: languages.response.error[this.state.language]});
        console.log('error add bank -->', error);
      });
      
    }
  }

  render(){
    return(
      <View style={[mystyles.page]}>

        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') } />
          
        <Breadcrumb 
          title={ languages.menu.addBank[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />

        <View style={ mystyles.page }>

          { this.state.error && <Label isError={true} message={this.state.error} /> }
          { this.state.success && <Label message={this.state.success} /> }

          <ScrollView>
            <View style={[mystyles.mh20]}>
              <MySelectInput 
                label={ languages.page.addBank.form.bank[this.state.language] }
                items={ this.state.banks }
                isRequiredMark={ true }
                onChangeSelect={ (bank) => this.setState({bank}) }
                value={ this.state.bank }
                error={ this.state.errors.bank } />

              <CurrencyOption
                text={ languages.page.addBank.form.currency[this.state.language] }
                onChange={ (currency, position) => this.setState({currency, flag: this.state.currencyList[position].flag}) } 
                value={ this.state.currency }
                flag={ this.state.flag }
                error={ this.state.errors.currency }
                items={ this.state.currencyList } />

              <MyTextInput 
                label={ languages.page.addBank.form.account[this.state.language] } 
                isRequiredMark={ true }
                value={ this.state.number }
                onChangeText={ (number) => this.setState({number}) }
                keyboardType="numeric" 
                error={ this.state.errors.number } />

              <MyTextInput 
                label={ languages.page.addBank.form.user[this.state.language] } 
                isRequiredMark={ true }
                value={ this.state.name }
                onChangeText={ (name) => this.setState({name}) }
                error={ this.state.errors.name } />

            </View>
          </ScrollView>
          <TouchableOpacity activeOpacity={0.8} onPress={() => this.save() }>
            <Text style={mystyles.formButton}>
              { languages.page.addBank.form.save[this.state.language] }
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}