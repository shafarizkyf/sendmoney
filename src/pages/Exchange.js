import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  CardExchange,
  Label
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import flag from '../config/flag';
import validate from 'validate.js';
import exchangeValidation from '../validation/ExchangeValidation';
import Axios from 'axios';
import api from '../config/api';
import languages from '../config/languages';
import currencies from '../config/currencies';

export default class Exchange extends Component {
  
  state = {
    language: 'id',
    modal: false,
    currencyOptions: [],
    currencyExchange: [
      {
        country:'indoneisa', 
        currency:'Indonesia Rupiah', 
        symbol:'IDR',
        icon: api.flagApi.replace('{country}', currencies['IDR']),
        value: null
      },
      {
        country:'english', 
        currency:'Dollar Amerika', 
        symbol:'USD',
        icon: api.flagApi.replace('{country}', currencies['USD']),
        value: null
      },
    ]
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    this.currencyList(); 
  }

  currencyList(code = ''){
    const data = {
      com: api.com.getCurrency,
      kodeMataUang: code,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;

      let currency = [];
      if(response.resultCode == api.response.success){
        currency = response.data.map(obj => {
          return {
            currency: obj.namaMataUang,
            symbol: obj.kodeMataUang,
            value: obj.kodeMataUang,
            icon: api.flagApi.replace('{country}', currencies[obj.kodeMataUang]),
          }
        });
        
        this.setState({currencyOptions: currency});
      }

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error exchange get list currency -->', error);
    });
  }

  _selectCurrency(index){
    let currencyExchange = this.state.currencyExchange;
    currencyExchange[this.state.selectedComponent] = this.state.currencyOptions[index];
    currencyExchange[this.state.selectedComponent].value = null;
    this.setState({modal: false, currencyExchange});
  }

  _onChangeText(index, val){
    let currencyExchange = this.state.currencyExchange;
    currencyExchange[index].value = val;
    this.setState({currencyExchange});
  }

  calculate(){
    let errors = validate({from:this.state.currencyExchange[0].value}, exchangeValidation);

    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      
      let data = {
        com: api.com.exchange,
        from: this.state.currencyExchange[0].symbol,
        to: this.state.currencyExchange[1].symbol,
        amount: this.state.currencyExchange[0].value,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password  
      }

      console.log('data -->', data);

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        console.log('exchange -->', response);
        if(response.resultCode == api.response.success){
          //return data like this --> 5000 IDR = 0.34807427999999996 USD
          let results = response.data.value; 

          let currencyExchange = this.state.currencyExchange;
          currencyExchange[1].value = results.split(" ")[3];
          this.setState({currencyExchange});
          
        }else if(response.resultCode == api.response.error){
          this.setState({error: 'No Result'});
        }

      }).catch(error => {
        this.setState({error: 'Internal Server Error'});
        console.log('error exchange -->', error);
      })
    }
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ language.exchange.breadcrumb[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        { this.state.error && <Label isError={true} message={this.state.error} /> }
        
        <KeyboardAvoidingView style={mystyles.page} behavior="padding" keyboardVerticalOffset={20}>
          <ScrollView>
            <View style={[ mystyles.mh20]}>

              {
                this.state.currencyExchange.map((obj, index) => {
                  let status = index == 0 ? 'from' : 'to';
                  return <CardExchange 
                    key={index}
                    index={index}
                    moneyIcon={ obj.icon }
                    moneyText={ obj.currency }
                    description={`1 ${this.state.currencyExchange[0].symbol} = 1 ${this.state.currencyExchange[1].symbol}`}
                    symbol={ obj.symbol }
                    onChangeExchange={ (index) => this.setState({modal: true, selectedComponent:index}) }
                    onSelectCurrency={ (optionIndex) => this._selectCurrency(optionIndex) }
                    onChangeText={ (index, val) => this._onChangeText(index, val) }
                    modalVisible={ this.state.modal }
                    closeModal={ () => this.setState({modal:false}) }
                    currencyOptions={ this.state.currencyOptions }
                    status={ status }
                    value={ this.state.currencyExchange[index].value }
                    errors={ this.state.errors }
                  />
                })
              }

            </View>
          </ScrollView>
        </KeyboardAvoidingView>

        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.calculate() } >
          <Text style={ mystyles.formButton }>{ language.exchange.button[this.state.language] }</Text>
        </TouchableOpacity>
      </View>
    );
  }
}