import React from 'react';
import { 
  View, 
  ImageBackground,
  ScrollView
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MenuButton
} from '../components';
import languages from '../config/languages';
import mystyles from '../styles/mystyles';

export default class Setting extends React.Component{
  
  state = {
    language: 'id'
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
  }

  render(){
    return(
      <View style={[mystyles.page]}>

        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.menu.setting[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <View style={[mystyles.page]}>
          <ImageBackground source={ require('../../assets/img/background.png') } style={ mystyles.loginBg } />
          <ScrollView style={[mystyles.mt20]}>
            <View style={[mystyles.mh20]}>
              <MenuButton 
                navigation={ () => this.props.navigation.navigate('currency') }
                label={ languages.menu.currency[this.state.language] } />

              <MenuButton 
                navigation={ () => this.props.navigation.navigate('bank') }
                label={ languages.menu.bank[this.state.language] } />

              <MenuButton 
                navigation={ () => this.props.navigation.navigate('security') }
                label={ languages.menu.securiy[this.state.language] } />

              <MenuButton 
                navigation={ () => this.props.navigation.navigate('userPassword') }
                label={ languages.menu.changePassword[this.state.language] } />
            </View>
          </ScrollView>
        </View>

      </View>
    );
  }
}