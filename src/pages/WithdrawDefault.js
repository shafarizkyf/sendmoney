import React from 'react';
import { 
  View, 
  Text, 
  ScrollView, 
  TouchableOpacity, 
  KeyboardAvoidingView 
} from 'react-native';
import { 
  CurrencyOption, 
  MyTextInput, 
  MySelectInput 
} from '../components';
import mystyles from '../styles/mystyles';
import flag from '../config/flag';
import languages from '../config/languages';

export default class WithdrawDefault extends React.Component {

  state = {
    banks: [{value: '', label:languages.blankOptions.bank[this.props.language]}],
    currencyList: [{value: '', label:languages.blankOptions.currency[this.props.language]}],
  };

  componentDidMount(){
    if(this.props.banks){
      this.setState({banks: this.props.banks});
    }

    if(this.props.currency){
      this.setState({currencyList: this.props.currency});
    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={[mystyles.mh20, mystyles.mt10]}>

            <CurrencyOption 
              text={ languages.page.withdraw.form.default.currency[this.props.language] }
              onChange={ (currency, position) => this.setState({currency, flag: this.state.currencyList[position].flag}) }
              value={ this.state.currency }
              flag={ this.state.flag }
              error={ this.props.errors.currency }
              items={ this.state.currencyList } />

            <MySelectInput 
              label={ languages.page.withdraw.form.default.bank[this.props.language] }
              items={ this.state.banks }
              isRequiredMark={ true }
              onChangeSelect={ (bank) => this.setState({bank}) }
              value={ this.state.bank }
              error={ this.props.errors.bank } />
      
            <MyTextInput 
              label={ languages.page.withdraw.form.default.account[this.props.language] }
              isRequiredMark={ true }
              keyboardType="numeric"
              value={this.state.number}
              onChangeText={ (number) => this.setState({number}) }
              error={ this.props.errors.number } />

            <MyTextInput 
              label={ languages.page.withdraw.form.default.amount[this.props.language] } 
              isRequiredMark={ true }
              value={ this.state.amount }
              onChangeText={ (amount) => this.setState({amount}) }
              keyboardType="numeric" 
              error={ this.props.errors.amount } />
      
            <MyTextInput 
              label={ languages.page.withdraw.form.default.note[this.props.language] } 
              value={ this.state.note }
              onChangeText={ (note) => this.setState({note}) }
              error={ this.props.errors.note } />
              
          </View>
        </ScrollView>
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.onProses(this.state) }>
          <Text style={mystyles.formButton}>{'Proses'}</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
  
}