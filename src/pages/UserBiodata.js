import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput,
  MyDateInput,
  Label
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import validate from 'validate.js';
import biodataValidation from '../validation/UserBiodataValidation';
import api from '../config/api';
import Axios from 'axios';

export default class UserBiodata extends Component {

  state = {
    language: 'id',
    errors: [],
    user: {}
  }

  async componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});

    let breadcrumb = await this.props.navigation.getParam('breadcrumb');
    let updateData = await this.props.navigation.getParam('updateData');
    let user = await this.props.navigation.getParam('user');
    console.log('is update biodata -->', updateData);

    breadcrumb = breadcrumb ? breadcrumb : language.register.step1.title[this.state.language];
    this.setState({breadcrumb, updateData, user: user ? user : {}});  
  }

  nextStep(){
    let errors = validate(this.state, biodataValidation);
    errors ? this.setState({errors}) : this.setState({errors:[]});

    if(!errors && !this.state.updateData){
      this.props.navigation.navigate('verify', {
        biodata: this.state
      });
      
    }else if(!errors && this.state.updateData){
      let data = {
        com: api.com.updateBiodata,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password,
        namaLengkap: this.state.name,
        tempatLahir: '',
        tanggalLahir: this.state.date,
        email: this.state.email,
        noHandphone: this.state.phone,
      }

      console.log('update biodata -->', data);

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        console.log('update biodata -->', response);

        if(response.resultCode == api.response.success){
          this.setState({success: language.response.saving.success[this.state.language]});

        }else if(response.resultCode == api.response.error){
          this.setState({error: language.response.saving.error[this.state.language]});
        }

      }).catch(error => {
        this.setState({error: language.response.error[this.state.language]});
        console.log('error update biodata -->', error);
      });

    }    
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
        
        <Breadcrumb 
          title={ this.state.breadcrumb } 
          onBack={ () => this.props.navigation.goBack() } />
        
        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <KeyboardAvoidingView style={[mystyles.form, mystyles.page]} behavior="padding" keyboardVerticalOffset={20}>
          <ScrollView showsVerticalScrollIndicator={false}>

            <MyTextInput 
              label={language.register.form.name[this.state.language] } 
              isRequiredMark={true}
              value={this.state.name ? this.state.name : this.state.user.name}
              onChangeText={ (val) => { this.setState({name:val}) } }
              error={this.state.errors.name} />

            <MyDateInput 
              label={language.register.form.date[this.state.language] } 
              isRequiredMark={true}
              value={this.state.date ? this.state.date : this.state.user.birthday}
              onChangeDate={ (val) => { this.setState({date:val}) } } 
              error={this.state.errors.date} />

            <MyTextInput 
              label={language.register.form.email[this.state.language] } 
              isRequiredMark={true}
              value={this.state.email ? this.state.email : this.state.user.email}
              onChangeText={ (val) => { this.setState({email:val}) } }
              keyboardType={'email-address'} 
              error={this.state.errors.email} />

            <MyTextInput 
              label={language.register.form.phone[this.state.language] } 
              isRequiredMark={true}
              value={this.state.phone ? this.state.phone : this.state.user.phone}
              onChangeText={ (val) => { this.setState({phone:val}) } }
              keyboardType={'phone-pad'} 
              error={this.state.errors.phone} />

          </ScrollView>
        </KeyboardAvoidingView>

        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.nextStep() } >
          <Text style={ mystyles.formButton }>
          { this.state.updateData ? language.register.form.buttonUpdate[this.state.language]  : language.register.form.button[this.state.language] }
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}