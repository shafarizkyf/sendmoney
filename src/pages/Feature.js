import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import {
  Navbar,
  TopMenu, 
  Footer,
  FeatureTableHeader,
  FeatureTableBody,
} from '../components';
import language from '../config/languages';
import mystyle from '../styles/mystyles';

export default class Feature extends React.Component {

  state = {
    language: 'id',
    tableHead: ['Layanan', 'Aktif', 'Terverifikasi'],
    tableBody: [
      ['Pendaftaran', true, true],
      ['Konversi Mata Uang', false, true],
      ['Deposit Ke Akun SendMoney', true, true],
      ['Transfer Antar Akun SendMoney', true, true],
      ['Transfer Ke Bank Nasional', false, true],
      ['Transfer Ke Bank Internasional ', false, true],
      ['Penarikan Ke Rekening Bank', false, true],
      ['Pin Keamanan Akses', true, true],
      ['Ekspor Mutasi Transaksi', true, true],
      ['API SendMoney', false, true],
      ['Live Chat', true, true],
    ]
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  render(){
    return (
      <View style={ mystyle.page }>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />

        <TopMenu 
          homePage={ () =>  this.props.navigation.navigate('home') }
          servicePage={ () => this.props.navigation.navigate('service') }
          featurePage={ () =>  this.props.navigation.navigate('feature') }
          pricingPage={ () =>  this.props.navigation.navigate('pricing') }
          language={ this.state.language } selected={ 3 } />
        
        <View style={[mystyle.page]}>
          <ScrollView>
            <View style={[mystyle.mh20]}>
              <Text style={ mystyle.pageTitle }>{ language.home.navbarMenu.feature[this.state.language] }</Text>
              <View style={ mystyle.borderGrey }>             
                <FeatureTableHeader data={this.state.tableHead} />
                { this.state.tableBody.map((obj, index) =>  <FeatureTableBody key={index} data={obj} index={index} />) }
              </View>
            </View>
          </ScrollView>          
          <Footer />
        </View>

      </View>
    );
  }
}