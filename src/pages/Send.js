import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Navbar,
  Breadcrumb,
  Balance,
  Tabs,
  Label
} from '../components';
import mystyles from '../styles/mystyles';
import { SendUser, SendBank, SendRiwayat } from '.';
import validate from 'validate.js';
import sendUserValidation from '../validation/SendUserValidation';
import sendBankValidation from '../validation/SendBankValidation';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';


export default class Send extends Component {

  state = {
    language: 'id',
    tab: 1,
    tab1: [],
    tab2: [],
    tabs: [
      {label: languages.page.send.tab.sendToUser[this.props.screenProps.selectedLanguage] , tabId: 1},
      {label: languages.page.send.tab.sendViaBank[this.props.screenProps.selectedLanguage], tabId: 2},
      {label: languages.page.send.tab.history[this.props.screenProps.selectedLanguage], tabId: 3},
    ],
    errors: [],
    history: []
  }
  
  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});

    this.history();
    this.banks();
    this.currency();
  }

  async banks(){
    if(this.props.screenProps.banks){
      this.setState({banks: [{value: '', label: languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.banks]});
    }else{
      await this.props.screenProps.getBanks();
      this.setState({banks: [{value: '', label: languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.banks]});
    }
  }
  
  async currency(){
    if(this.props.screenProps.currency){
      this.setState({currency: [{value: '', label: languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }else{
      await this.props.screenProps.getCurrency();
      this.setState({currency: [{value: '', label: languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }
  }

  async onProses(obj){
    let sendRules = {};
    if(this.state.tab === 1){
      sendRules = sendUserValidation;
      await this.setState({tab1: obj});

    }else if(this.state.tab === 2){
      sendRules = sendBankValidation;
      await this.setState({tab2: obj});
    }

    let errors = validate(obj, sendRules);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
    }

    if(!errors && this.state.tab === 1){
      this.proceedSendMoneyToUser();
      
    }else if(!errors && this.state.tab === 2){
      this.proceedBankTransfer();
    }

  }

  proceedSendMoneyToUser(){
    let data = {
      com: api.com.sendMoneyToUser,
      mataUang: this.state.tab1.currency,
      userIdTujuan: this.state.tab1.to,
      kodeBankTujuan: '',
      rekeningTujuan: '',
      jumlah: this.state.tab1.amount,
      keterangan: this.state.tab1.note,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    console.log('send money to user -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      this.setMessage(response);
      console.log('send money to user -->', response);

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error send money to user -->', error);
    });
  }

  proceedBankTransfer(){
    let data = {
      com: api.com.sendBankTransfer,
      mataUang: this.state.tab2.currency,
      userIdTujuan: '',
      kodeBankTujuan: this.state.tab2.bank,
      rekeningTujuan: this.state.tab2.number,
      jumlah: this.state.tab2.amount,
      keterangan: this.state.tab2.note,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    console.log('send money via bank -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      this.setMessage(response);
      console.log('send money via bank transfer -->', response);

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error send money via bank transfer -->', error);
    });
  }

  setMessage(response){
    switch(response.resultCode){
      case api.response.success:
        this.setState({error: null, success: languages.response.transaction.success[this.state.language]});
      break;

      case api.response.empty:
        this.setState({
          success:null, 
          error: response.data ? response.data : languages.response.empty[this.state.language]
        });
      break;

      case api.response.error:
        this.setState({success:null, error: languages.response.transaction.error[this.state.language]});
      break;
    }
  }

  history(){
    let data = {
      com: api.com.sendMoneyRiwayat,
      dateFrom: '2018-10-01 00:00:00',
      dateTo: '2018-12-31 00:00:00',
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password
    };
    
    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('send money riwayat -->', response);

      let history = [];
      if(response.resultCode == api.response.success){
        history = response.data.map(obj => {
          return {
            date: obj.tanggal,
            userId: obj.userId,
            currency: obj.mataUang,
            transactionType: obj.tujuan,
            method: obj.metode,
            amount: obj.jumlah,
            note: obj.keterangan,
            status: obj.status,
            type: obj.status == 'Sedang Diproses' ? 'pending' : 'success',
            expire: obj.batasDeposit,
            account: `${obj.rekening} - ${$obj.atasNama}`
          }
        });
      }

      this.setState({history});

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error history deposit -->', error);
    })
  }

  render(){

    function TabContent(props){
      if(props.tab == 1){
        return <SendUser
        language={props.language}
        user={props.user}
        currency={props.currency}
        onProses={(obj) => props.onProses(obj)} 
        errors={props.errors} values={props.tab1} />

      }else if(props.tab == 2){
        return <SendBank
        language={props.language}
        banks={props.banks}
        user={props.user}
        currency={props.currency}
        onProses={(obj) => props.onProses(obj)} 
        errors={props.errors} 
        values={props.tab2} />

      }else if(props.tab == 3){
        return <SendRiwayat
        language={props.language}
        user={props.user}
        history={props.history}
        onProses={(obj) => props.onProses(obj)} 
        detailPage={props.detailPage} />
      }
    }

    return(
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
        
        <Breadcrumb 
          title={ languages.menu.transfer[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <Balance data={ this.props.screenProps.balance } />

        <Tabs
          tabs={ this.state.tabs } 
          selected={ this.state.tab } 
          navigation={ (val) => this.setState({ tab:val }) } />
        
        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <TabContent
          language={ this.state.language }
          banks= { this.state.banks }
          currency={ this.state.currency }
          history={ this.state.history }
          user={ this.props.screenProps.user }
          onProses={ (obj) => this.onProses(obj) }
          detailPage={ () => this.props.navigation.navigate('sendDetail') }
          errors={ this.state.errors }
          tab={ this.state.tab } 
          tab1={ this.state.tab1 }
          tab2={ this.state.tab2 } />
      </View>
    );
  }
}