import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MySelectInput,
  MyTextInput,
  MyDateInput,
  Label,
  MyImageInput
} from '../components';
import { ImagePicker } from 'expo';
import mystyles from '../styles/mystyles';
import validate from 'validate.js';
import userVerifyIdentity from '../validation/UserVerifyIdentityValidation';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';

export default class UserVerifyIdentity extends React.Component {

  state = {
    language: 'id',
    types:[
      {label: languages.blankOptions.identityType[this.props.screenProps.selectedLanguage], value:''},
      {label: languages.page.userDocument.form.citizenId[this.props.screenProps.selectedLanguage], value:'ktp'},
      {label: languages.page.userDocument.form.driverLisence[this.props.screenProps.selectedLanguage], value:'sim'},
      {label: languages.page.userDocument.form.passport[this.props.screenProps.selectedLanguage], value:'passpor'},
    ],
    errors: [],
    image: [],
    user: {}
  };

  async componentDidMount(){
    let user = await this.props.navigation.getParam('user');
    this.setState({language: this.props.screenProps.selectedLanguage, user});
  }

  async pickDocument() {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [1, 1],
      base64: true
    });

    console.log('selected image -->', result)
    this.setState({image: result});  
  }

  
  save(){
    let errors = validate(this.state, userVerifyIdentity);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      
      let data = {
        com: api.com.updateIdentity,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password,
        statusUser: null, //1 aktif, 0 tidak aktif
        tipeIdentitas: this.state.type,
        nomorIdentitas: this.state.number,
        fotoIdentitas: this.state.image.base64, //byte (mungkin maksudnya base64)
        berlakuHingga: this.state.expire,
        keterangan: this.state.note,
        statusIdentitas: null, //1 aktif, 0 tidak aktif
      }

      console.log('update identity -->', data);

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        console.log('update identity -->', response);

        if(response.resultCode == api.response.success){
          this.setState({success: 'Berhasil memperbarui data'});
        
        }else if(response.resultCode == api.response.error){
          this.setState({error: 'Ups! Terjadi kesalahan saat memperbarui data'});
        }

      }).catch(error => {
        console.log('error update identity -->', error);
      });

    }
  }

  render(){
    return(
      <View style={ mystyles.page }>
      
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.page.userDocument.form.identityVerification[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <ScrollView>
          <View style={[mystyles.mh20]}>
            <MySelectInput 
              label={ languages.page.userDocument.form.identityType[this.state.language] }
              items={ this.state.types }
              isRequiredMark={ true }
              onChangeSelect={ (type) => this.setState({type}) }
              value={ this.state.type }
              error={ this.state.errors.type } />

            <MyTextInput 
              label={ languages.page.userDocument.form.identityNumber[this.state.language] } 
              isRequiredMark={ true }
              value={ this.state.number ? this.state.number : this.state.user.identityNumber }
              onChangeText={ (number) => this.setState({number}) }
              keyboardType="numeric" 
              error={ this.state.errors.number } />
            
            <MyImageInput
              label={ languages.page.userDocument.form.identityImage[this.state.language] }
              isRequiredMark={ true } 
              onSelectImage={ () => this.pickDocument() } />
      
            <MyDateInput 
              label={ languages.page.userDocument.form.validity[this.state.language] }
              isRequiredMark={ true }
              value={ this.state.expire ? this.state.expire : this.state.user.validity }
              onChangeDate={ (expire) => this.setState({expire}) }
              error={ this.state.errors.expire } />

            <MyTextInput 
              label={ languages.page.userDocument.form.note[this.state.language] } 
              value={ this.state.note ? this.state.note : this.state.user.note }
              onChangeText={ (note) => this.setState({note}) }
              error={ this.state.errors.note } />
          </View>
        </ScrollView>
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.save() }>
          <Text style={mystyles.formButton}>
            { languages.page.userDocument.form.upload[this.state.language] }
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
  
}