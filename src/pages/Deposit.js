import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Navbar,
  Breadcrumb,
  Balance,
  Tabs,
  Label
} from '../components';
import mystyles from '../styles/mystyles';
import DepositOtomatis from './DepositOtomatis';
import { DepositManual, DepositRiwayat } from '.';
import validate from 'validate.js';
import depositOtomatisValidation from '../validation/DepositOtomatisValidation';
import depositManualValidation from '../validation/DepositManualValidation';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';

export default class Deposit extends Component {

  state = {
    language: 'id',
    tab: 1,
    tab1: [],
    tab2: [],
    tabs: [
      {label: languages.page.deposit.tab.manual[this.props.screenProps.selectedLanguage], tabId: 1},
      {label: languages.page.deposit.tab.automatic[this.props.screenProps.selectedLanguage], tabId: 2},
      {label: languages.page.deposit.tab.history[this.props.screenProps.selectedLanguage], tabId: 3},
    ],
    errors: [],
    history: []
  }
  
  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    
    this.history();
    this.userBanks();
    this.currency();
    this.methodList();
  }

  async userBanks(){
    if(this.props.screenProps.userBanks){
      this.setState({banks: [{value: '', label:languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.userBanks]});
    }else{
      await this.props.screenProps.getUserBanks();
      this.setState({banks: [{value: '', label:languages.blankOptions.bank[this.state.language]}, ...this.props.screenProps.userBanks]});
    }
  }
  
  async currency(){
    if(this.props.screenProps.currency){
      this.setState({currency: [{value: '', label:languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }else{
      await this.props.screenProps.getCurrency();
      this.setState({currency: [{value: '', label:languages.blankOptions.currency[this.state.language]}, ...this.props.screenProps.currency]});
    }
  }

  async methodList(){
    await this.props.screenProps.getDepositMethod();
    if(this.props.screenProps.depositMethod){
      this.setState({methodList: [{value: '', label:languages.blankOptions.paymentMethod[this.state.language]}, ...this.props.screenProps.depositMethod]});
    }
  }


  async onProses(obj){
    
    let depositRules = {};
    if(this.state.tab === 1){
      depositRules = depositOtomatisValidation;
      await this.setState({tab1: obj});
      console.log('deposit otomatis -->', obj);

    }else if(this.state.tab === 2){
      depositRules = depositManualValidation;
      await this.setState({tab2: obj});
      console.log('deposit manual -->', obj);
    }

    let errors = validate(obj, depositRules);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
    }

    if(!errors && this.state.tab === 1){
      this.proceedDepositOtomatis();      
    
    }else if(!errors && this.state.tab === 2){
      this.proceedDepositManual();
    }

  }
  
  proceedDepositOtomatis(){
    let data = {
      com: api.com.depositOtomatis,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
      mataUang: this.state.tab1.currency,
      trxOtomatis: 1,
      kodeBank: this.state.tab1.bank,
      metode: null,
      jumlah: this.state.tab1.amount,
      keterangan: this.state.tab1.note
    };

    console.log('data deposit otomatis -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      this.setMessage(response.resultCode);
      console.log('deposit otomatis -->', response);

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error deposit otomatis -->', error);
    });
  }

  proceedDepositManual(){
    let data = {
      com: api.com.depositManual,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
      mataUang: this.state.tab2.currency,
      trxOtomatis: 0,
      kodeBank: null,
      metode: this.state.tab2.method,
      jumlah: this.state.tab2.amount,
      keterangan: this.state.tab2.note
    };

    console.log('data deposit manual', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      this.setMessage(response.resultCode);
      console.log('deposit manual -->', response);

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error deposit manual -->', error);
    });
  }

  history(){
    let data = {
      com: api.com.depositRiwayat,
      dateFrom: '2018-01-01 00:00:00',
      dateTo: '2018-12-31 00:00:00',
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password
    };
    
    console.log('data deposit riwayat -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('deposit riwayat -->', response);

      let history = [];
      if(response.resultCode == api.response.success){
        history = response.data.map(obj => {
          return {
            date: obj.tanggal,
            userId: obj.userId,
            currency: obj.mataUang,
            transactionType: obj.otomatis == 'true' ? languages.page.deposit.tab.automatic[this.state.language] : languages.page.deposit.tab.automatic[this.state.language],
            method: obj.metode,
            amount: obj.jumlah,
            note: obj.keterangan,
            status: obj.status,
            type: obj.status == 'Sedang Diproses' ? 'pending' : 'success',
            expire: obj.batasDeposit,
            account: obj.rekening == 'null' ? '-' : obj.rekening
          }
        });
      }

      this.setState({history});

    }).catch(error => {
      this.setState({error: languages.response.error[this.state.language]});
      console.log('error history deposit -->', error);
    })
  }

  setMessage(resultCode){
    switch(resultCode){
      case api.response.success:
        this.setState({error: null, success: languages.response.transaction.success[this.state.language]});
      break;

      case api.response.empty:
        this.setState({success:null, error: languages.response.empty[this.state.language]});
      break;

      case api.response.error:
        this.setState({success:null, error: languages.response.transaction.error[this.state.language]});
      break;
    }
  }

  render(){

    function TabContent(props){
      if(props.tab == 1){
        return <DepositOtomatis
        language={ props.language }
        banks={ props.banks }
        currency={ props.currency }
        onProses={ (obj) => props.onProses(obj) } 
        errors={ props.errors } 
        values={ props.tab1 } />

      }else if(props.tab == 2){
        return <DepositManual
        language={ props.language }
        banks={ props.banks }
        currency={ props.currency }
        methodList={ props.methodList }
        onProses={ (obj) => props.onProses(obj) } 
        errors={ props.errors } 
        values={ props.tab2 } />
        
      }else if(props.tab == 3){
        return <DepositRiwayat 
        language={ props.language } 
        history={ props.history } />
      }
    }

    return(
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.menu.deposit[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <Balance data={ this.props.screenProps.balance } />

        <Tabs 
          tabs={this.state.tabs}
          selected={this.state.tab} 
          navigation={(tab) => { this.setState({ tab }) }}  />

        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <TabContent
          banks={ this.state.banks }
          currency={ this.state.currency }
          methodList={ this.state.methodList }
          history={ this.state.history }
          tab={ this.state.tab } 
          onProses={ (obj) => this.onProses(obj) }
          errors={ this.state.errors }
          tab1={ this.state.tab1 }
          tab2={ this.state.tab2 }
          language={ this.state.language } />
      </View>
    );
  }
}