import React from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput,
  Label
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import validate from 'validate.js';
import userAddressValidation from '../validation/UserAddressValidation';
import api from '../config/api';
import Axios from 'axios';

export default class UserAddress extends React.Component {
  
  state = {
    language: 'id',
    user: {},
    errors: []
  }

  async componentDidMount(){
    let user = await this.props.navigation.getParam('user');
    this.setState({language: this.props.screenProps.selectedLanguage, user});
  }

  save(){
    let errors = validate(this.state, userAddressValidation);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      
      let data = {
        com: api.com.updateAddress,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password,
        alamat: this.state.address,
        kota: this.state.city,
        provinsi: this.state.province,
        kodePos: this.state.zipcode,
        negara: this.state.country
      }

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        console.log('update biodata -->', response);

        if(response.resultCode == api.response.success){
          this.setState({success: 'Berhasil memperbarui data'});

        }else if(response.resultCode == api.response.error){
          this.setState({error: 'Ups! Terjadi kesalahan saat memperbarui data'});
        }

      }).catch(error => {
        console.log('error update address -->', error);
      })

    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>

        <Navbar
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
        
        <Breadcrumb 
          title={ language.menu.addressData[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />
      
        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <ScrollView showsVerticalScrollIndicator={false}>

          <View style={[mystyles.form, { flex:1 }]}>

            <MyTextInput 
              label={language.register.form.address[this.state.language] } 
              isRequiredMark={true}
              value={this.state.address ? this.state.address : this.state.user.address}
              onChangeText={ (val) => { this.setState({address:val}) } }
              inputWrapperStyle={mystyles.mt0}
              multiline={true} 
              error={this.state.errors.address} />

            <MyTextInput 
              label={language.register.form.city[this.state.language] } 
              isRequiredMark={true}
              value={this.state.city ? this.state.city : this.state.user.city}
              onChangeText={ (val) => { this.setState({city:val}) } }
              inputWrapperStyle={mystyles.mt0}
              error={this.state.errors.city} />

            <MyTextInput 
              label={language.register.form.province[this.state.language] } 
              isRequiredMark={true}
              value={this.state.province ? this.state.province : this.state.user.province}
              onChangeText={ (val) => { this.setState({province:val}) } }
              inputWrapperStyle={mystyles.mt0}
              error={this.state.errors.province} />

            <MyTextInput 
              label={language.register.form.zipcode[this.state.language] } 
              isRequiredMark={true}
              value={this.state.zipcode ? this.state.zipcode : this.state.user.zipcode}
              onChangeText={ (val) => { this.setState({zipcode:val}) } }
              inputWrapperStyle={mystyles.mt0}
              keyboardType="numeric"
              error={this.state.errors.zipcode} />

            <MyTextInput 
              label={language.register.form.country[this.state.language] } 
              isRequiredMark={true}
              value={this.state.country ? this.state.country : this.state.user.country}
              onChangeText={ (val) => { this.setState({country:val}) } }
              inputWrapperStyle={mystyles.mt0}
              error={this.state.errors.country} />

          </View>

        </ScrollView>

        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.save() } >
          <Text style={ mystyles.formButton }>
            { language.register.form.buttonUpdate[this.state.language] }
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }

}