import Home from './Home';
import Service from './Service';
import Feature from './Feature';
import Pricing from './Pricing';
import Login from './Login';
import UserBiodata from './UserBiodata';
import UserVerify from './UserVerify';
import UserAddress from './UserAddress';
import MainMenu from './MainMenu';
import Exchange from './Exchange';
import Deposit from './Deposit';
import DepositOtomatis from './DepositOtomatis';
import DepositManual from './DepositManual';
import DepositRiwayat from './DepositRiwayat';
import Send from './Send';
import SendUser from './SendUser';
import SendBank from './SendBank';
import SendRiwayat from './SendRiwayat';
import SendDetail from './SendDetail';
import Withdraw from './Withdraw';
import WithdrawDefault from './WithdrawDefault';
import WithdrawRiwayat from './WithdrawRiwayat';
import Notification from './Notification';
import Profile from './Profile';
import Setting from './Setting';
import UserVerifyIdentity from './UserVerifyIdentity';
import UserPassword from './UserPassword';
import UserAddressAndSecurity from './UserAddressAndSecurity';
import UserVerifyPhoneNumber from './UserVerifyPhoneNumber';
import AddBank from './AddBank';
import Security from './Security';
import Mutation from './Mutation';
import Currency from './Currency';
import Bank from './Bank';
import PrivacyPolicy from './PrivacyPolicy';
import TermsAndConditions from './TermsAndConditions';
import AboutUs from './AboutUs';
import ForgetPassword from './ForgetPassword';

export {
  Home,
  Service,
  Feature,
  Pricing,
  Login,
  UserBiodata,
  UserVerify,
  UserAddress,
  MainMenu,
  Exchange,
  Deposit,
  DepositOtomatis,
  DepositManual,
  DepositRiwayat,
  Send,
  SendUser,
  SendBank,
  SendRiwayat,
  SendDetail,
  Withdraw,
  WithdrawDefault,
  WithdrawRiwayat,
  Notification,
  Profile,
  Setting,
  UserVerifyIdentity,
  UserPassword,
  UserAddressAndSecurity,
  UserVerifyPhoneNumber,
  AddBank,
  Security,
  Mutation,
  Currency,
  Bank,
  PrivacyPolicy,
  TermsAndConditions,
  AboutUs,
  ForgetPassword
}