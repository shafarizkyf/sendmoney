import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import mystyles from '../styles/mystyles';
import { DepositRiwayatCard } from '../components';

const DepositRiwayat = (props) => (
  <View style={[ mystyles.page, mystyles.bgGrey ]}>
    <ScrollView showsVerticalScrollIndicator={false}>
      { props.history.map((obj,index) => <DepositRiwayatCard key={index} data={obj} type="pending" />) }
    </ScrollView>
  </View>
)

export default DepositRiwayat;