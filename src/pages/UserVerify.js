import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput,
  Label
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import Axios from 'axios';
import api from '../config/api';
import validate from 'validate.js';
import verifyValidation from '../validation/UserVerifyValidation';

export default class UserVerify extends Component {

  state = {
    language: 'id',
    isOtpSend: false,
    errors: []
  }

  async componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    let biodata = this.props.navigation.getParam('biodata');
    
    if(biodata){
      await this.setState({email: biodata.email, biodata});
      this.sendEmailOtp();
    }else{
      this.props.navigation.navigate('biodata');
    }
  }

  sendEmailOtp(){
    let data = {
      com: api.com.emailOtp,
      email: this.state.email
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      if(response.resultCode == api.response.success){
        this.setState({isOtpSend:true});
      }else{
        this.setState({error: language.response.emailOtp.error[this.state.language]});
      }

    }).catch(error => {
      this.setState({error: language.response.error[this.state.language]});
      console.log('error send email otp -->', error);
    });
  }

  nextStep(){
    let data = {
      com: api.com.getOtp,
      email: this.state.email,
      otp: this.state.otp
    };

    let errors = validate(this.state, verifyValidation);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        if(response.resultCode == api.response.success){
          this.props.navigation.navigate('addressAndSecurity', {
            biodata: this.state.biodata
          });
          
        }else{
          console.log('user verify -->', response);
          this.setState({error: language.response.emailOtp.invalid[this.state.language]});
        }
  
      }).catch(error => {
        this.setState({error: language.response.error[this.state.language]});
        console.log('error otp verification -->', error);
      });
    }
    
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>

        <Breadcrumb 
          title={ language.register.step2.title[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />
        
        { this.state.error && <Label isError={true} message={this.state.error} /> }

        <KeyboardAvoidingView style={[mystyles.form, mystyles.page]} behavior="padding" keyboardVerticalOffset={20}>
          <ScrollView showsVerticalScrollIndicator={false}>

            {
              this.state.isOtpSend &&
              <Text style={[ mystyles.alertSuccess, mystyles.textGreen, mystyles.f12]}>
                { language.register.form.verificationAlert[this.state.language] }
                <Text style={[mystyles.bold, mystyles.textGreen, mystyles.f12]}>
                  { ' '+language.register.form.verification[this.state.language] }
                </Text>
              </Text>
            }

            <MyTextInput 
              label={language.register.form.email[this.state.language] } 
              isRequiredMark={true}
              value={this.state.email}
              onChangeText={ (val) => { this.setState({email:val}) } }
              keyboardType={'email-address'}
              disabled={true} />
            
            <Text style={[mystyles.labelSend]} onPress={() => this.sendEmailOtp()}>
              { language.response.emailOtp.resend[this.state.language] }
            </Text>

            <MyTextInput 
              label={language.register.form.verification[this.state.language] } 
              isRequiredMark={true}
              value={this.state.otp}
              onChangeText={ (val) => { this.setState({otp:val}) } }
              keyboardType={'numeric'}
              error={this.state.errors.otp} />

          </ScrollView>
        </KeyboardAvoidingView>

        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.nextStep() }>
          <Text style={ mystyles.formButton }>{ language.register.form.button[this.state.language] }</Text>
        </TouchableOpacity>        
      </View>
    );
  }
}