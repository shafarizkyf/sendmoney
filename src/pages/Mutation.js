import React, { Component } from 'react';
import { 
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Alert
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  TableHead,
  TableBody,
  Label,
} from '../components';
import DatePicker from 'react-native-datepicker';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';
import languages from '../config/languages';
import api from '../config/api';
import Axios from 'axios';

export default class Mutation extends Component {
  
  state = {
    language: 'id',
    tableHeader: [
      languages.page.mutation.tableHeader.date[this.props.screenProps.selectedLanguage], 
      languages.page.mutation.tableHeader.type[this.props.screenProps.selectedLanguage], 
      languages.page.mutation.tableHeader.currency[this.props.screenProps.selectedLanguage], 
      languages.page.mutation.tableHeader.receive[this.props.screenProps.selectedLanguage], 
      languages.page.mutation.tableHeader.expend[this.props.screenProps.selectedLanguage], 
      languages.page.mutation.tableHeader.status[this.props.screenProps.selectedLanguage]],
    tableBody: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
  }

  onProses(){
    let data = {
      com: api.com.mutation,
      dateFrom: `${this.state.dateFrom}:00`,
      dateTo: `${this.state.dateTo}:00`,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password
    }

    console.log('mutation request -->', data);

    Axios.post(api.baseUrl, data)
    .then(response => {
      response = response.data;
      console.log('mutation data -->', response);

      if(response.resultCode == api.response.empty){
        Alert.alert('Hasil pencarian', 'Tidak ditemukan data transaksi');
      }

      let mutation = [];
      if(response.resultCode == api.response.success){
        mutation = response.data.map(obj => {
          return [
            obj.tanggal,
            obj.jenisTransaksi,
            obj.mataUang,
            obj.danaMasuk ? obj.danaMasuk : '-',
            obj.danaKeluar ? obj.danaKeluar: '-',
            obj.status
          ]
        });
      }

      this.setState({tableBody:mutation});

    }).catch(error => {
      console.log('error mutation -->', error);
    });

  }

  render(){
    return (
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>

        <Breadcrumb 
          title={ languages.menu.mutation[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <ScrollView>
          <View style={[mystyles.mh20]}>
            <View style={[mystyles.row]}>
              <Text style={[mystyles.bold, mystyles.textNavi, mystyles.selfCenter, mystyles.mr10, {width:80}]}>
                { languages.page.mutation.form.dateFrom[this.state.language] }
              </Text>
              <View style={[mystyles.page]}>
                <View style={[{alignItems:'flex-start', width:'50%'}]}>
                  <DatePicker 
                    placeholder="mm/dd/yyyy"
                    showIcon={ false }
                    style={{ width:'100%' }}
                    onDateChange={ (val) => this.setState({dateFrom:val}) }
                    date={ this.state.dateFrom }
                    mode="datetime"
                    customStyles={{
                      dateInput:{
                        alignItems: 'flex-start',
                        paddingHorizontal: 10,
                        height: 25,
                        borderColor: colors.black
                      },
                      placeholderText:{
                        color: colors.grey
                      }
                    }} 
                  />
                </View>
              </View>            
            </View>

            <View style={[mystyles.row]}>
              <Text style={[mystyles.bold, mystyles.textNavi, mystyles.selfCenter, mystyles.mr10, {width:80}]}>
                { languages.page.mutation.form.dateTo[this.state.language] }
              </Text>
              <View style={[mystyles.page]}>
                <View style={[{alignItems:'flex-start', width:'50%'}]}>
                  <DatePicker 
                    placeholder="mm/dd/yyyy"
                    showIcon={ false }
                    style={{ width:'100%' }}
                    onDateChange={ (val) => this.setState({dateTo:val}) }
                    date={ this.state.dateTo }
                    mode="datetime"
                    customStyles={{
                      dateInput:{
                        alignItems: 'flex-start',
                        paddingHorizontal: 10,
                        height: 25,
                        borderColor: colors.black
                      },
                      placeholderText:{
                        color: colors.grey
                      }
                    }} />
                </View>
              </View>            
            </View>
          </View>
          
          <View>
            <ScrollView>
              <TableHead data={this.state.tableHeader} />
              {
                this.state.tableBody.map((obj, index) => <TableBody key={index} data={obj} index={index} />)
              }
            </ScrollView>
          </View>
        </ScrollView>  
        <TouchableOpacity activeOpacity={0.8} onPress={() => this.onProses() }>
          <Text style={mystyles.formButton}>
            { languages.page.mutation.form.proceed[this.state.language] }
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}