import React from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput,
  Label
} from '../components';
import mystyles from '../styles/mystyles';
import validate from 'validate.js';
import userPasswordValidation from '../validation/UserPasswordValidation';
import api from '../config/api';
import Axios from 'axios';
import languages from '../config/languages';

export default class UserPassword extends React.Component {

  state = {
    language: 'id',
    errors: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
  }

  save(){
    let errors = validate(this.state, userPasswordValidation);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});

      let data = {
        com: api.com.changePassword,
        userId: this.props.screenProps.user.id,
        password: this.props.screenProps.user.password,
        "password lama": this.state.currentPassword,
        "password baru": this.state.newPassword
      }

      console.log('change password', data);

      Axios.post(api.baseUrl, data)
      .then(response => {
        response = response.data;
        console.log('change password -->', response.data);

        if(response.resultCode == api.response.success){
          this.setState({success: languages.response.saving.success[this.state.language]});
        }else{
          this.setState({error: languages.response.saving.error[this.state.language]});
        }
        
      }).catch(errors => {
        this.setState({error: languages.response.error[this.state.language]})
        console.log('error change password -->', errors);
      });

    }
  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" enabled>

        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') } />
          
        <Breadcrumb 
          title={ languages.menu.changePassword[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />

        { this.state.error && <Label isError={true} message={this.state.error} /> }
        { this.state.success && <Label message={this.state.success} /> }

        <ScrollView>
          <View style={[mystyles.mh20, mystyles.mt20]}>
            <MyTextInput 
              label={ languages.page.changePassword.form.currentPassword[this.state.language] } 
              isRequiredMark={ true }
              value={ this.state.currentPassword }
              onChangeText={ (currentPassword) => this.setState({currentPassword}) }
              isPasswordField={ true }
              inputWrapperStyle={ mystyles.mt0 } 
              error={ this.state.errors.currentPassword } />

            <MyTextInput 
              label={ languages.page.changePassword.form.newPassword[this.state.language] }
              isRequiredMark={ true }
              value={ this.state.newPassword }
              onChangeText={ (newPassword) => this.setState({newPassword}) }
              isPasswordField={ true }
              inputWrapperStyle={ mystyles.mt0 } 
              error={ this.state.errors.newPassword } />

            <MyTextInput 
              label={ languages.page.changePassword.form.confirmPassword[this.state.language] }
              isRequiredMark={ true }
              value={ this.state.confirmPassword }
              onChangeText={ (confirmPassword) => this.setState({confirmPassword}) }
              isPasswordField={ true }
              inputWrapperStyle={ mystyles.mt0 } 
              error={ this.state.errors.confirmPassword } />

          </View>
        </ScrollView>
        
        <TouchableOpacity activeOpacity={0.8} onPress={ () => this.save() }>
          <Text style={ mystyles.formButton }>
            { languages.page.changePassword.form.update[this.state.language] }
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }

}