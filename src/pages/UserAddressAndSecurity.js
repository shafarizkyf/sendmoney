import React, { Component } from 'react';
import { 
  View, 
  Text,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  MyTextInput,
  MyCheckBoxInput,
} from '../components';
import language from '../config/languages';
import mystyles from '../styles/mystyles';
import api from '../config/api';
import Axios from 'axios';
import validate from 'validate.js';
import addressAndSecurityValidation from '../validation/UserAddressAndSecurityValidation';
import languages from '../config/languages';

export default class UserAddress extends Component {
  
  state = {
    language: 'id',
    errors: [],
    agreement: false,
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    let biodata = this.props.navigation.getParam('biodata');
    biodata ? this.setState({biodata}) : this.props.navigation.navigate('biodata');
  }

  nextStep(){
    console.log(this.state);

    let dataRegister = {
      com: api.com.register,
      id: null,
      namaLengkap: this.state.biodata.name,
      tempatLahir: '',
      tanggalLahir: this.state.biodata.date,
      email: this.state.biodata.email,
      noHandphone: this.state.biodata.phone,
      alamat: this.state.address,
      kota: this.state.city,
      provinsi: this.state.province,
      kodePos: this.state.zipcode,
      negara: this.state.country,
      password: this.state.password,
      pertanyaanKeamanan: this.state.securtyQuestion,
      jawaban: this.state.securtyAnswer,
    };

    let errors = validate(this.state, addressAndSecurityValidation);
    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});
      Axios.post(api.baseUrl, dataRegister).then(response => {
        response = response.data;
        if(response.resultCode == api.response.success){
          this.props.screenProps.isLoggedIn = true;
          this.props.navigation.navigate('login');

        }else if(response.resultCode == api.response.error){
          this.setState({error: languages.response.saving.error[this.state.language]});
          console.log('user address and security -->', response);
        }
  
      }).catch(error => {
        this.setState({error: languages.response.error[this.state.language]});
        console.log('error register user -->', error);
      });
    }

  }

  render(){
    return(
      <KeyboardAvoidingView style={ mystyles.page } behavior="padding" keyboardVerticalOffset={20}>
      
        <Navbar
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
        
        <Breadcrumb 
          title={ language.register.step3.title[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() } />

        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={[ mystyles.formSectionWarning, mystyles.mb0 ]}>
            { language.register.form.securtySection[this.state.language] }
          </Text>

          <View style={[mystyles.form, mystyles.page]}>
            <MyTextInput 
              label={language.register.form.password[this.state.language] } 
              isRequiredMark={true}
              value={this.state.password}
              onChangeText={ (val) => { this.setState({password:val}) } }
              isPasswordField={true}
              inputWrapperStyle={mystyles.mt0} 
              error={this.state.errors.password} />

            <MyTextInput 
              label={language.register.form.confirmPassword[this.state.language] } 
              isRequiredMark={true}
              value={this.state.confirmPassword}
              onChangeText={ (val) => { this.setState({confirmPassword:val}) } }
              isPasswordField={true} 
              error={this.state.errors.confirmPassword} />

            <MyTextInput 
              label={language.register.form.securtyQuestion[this.state.language] } 
              isRequiredMark={true}
              value={this.state.securtyQuestion}
              onChangeText={ (val) => { this.setState({securtyQuestion:val}) } } 
              error={this.state.errors.securtyQuestion} />

            <MyTextInput 
              label={language.register.form.securtyAnswer[this.state.language] } 
              isRequiredMark={true}
              value={this.state.securtyAnswer}
              onChangeText={ (val) => { this.setState({securtyAnswer:val}) } } 
              error={this.state.errors.securtyAnswer} />
          </View>

          <Text style={[ mystyles.formSectionWarning, mystyles.mb0 ]}>
            { language.register.form.addressSection[this.state.language] }
          </Text>

          <View style={[mystyles.form, mystyles.page]}>

            <MyTextInput 
              label={language.register.form.address[this.state.language] } 
              isRequiredMark={true}
              value={this.state.address}
              onChangeText={ (val) => { this.setState({address:val}) } }
              inputWrapperStyle={mystyles.mt0}
              multiline={true} 
              error={this.state.errors.address} />

            <MyTextInput 
              label={language.register.form.city[this.state.language] } 
              isRequiredMark={true}
              value={this.state.city}
              onChangeText={ (val) => { this.setState({city:val}) } } 
              error={this.state.errors.city} />

            <MyTextInput 
              label={language.register.form.province[this.state.language] } 
              isRequiredMark={true}
              value={this.state.province}
              onChangeText={ (val) => { this.setState({province:val}) } } 
              error={this.state.errors.province} />
 
            <MyTextInput 
              label={language.register.form.zipcode[this.state.language] } 
              isRequiredMark={true}
              value={this.state.zipcode}
              onChangeText={ (val) => { this.setState({zipcode:val}) } }
              keyboardType={'numeric'} 
              error={this.state.errors.zipcode} />

            <MyTextInput 
              label={language.register.form.country[this.state.language] } 
              isRequiredMark={true}
              value={this.state.country}
              onChangeText={ (val) => { this.setState({country:val}) } } 
              error={this.state.errors.country} />

            <MyCheckBoxInput
              label={ language.register.form.agreement[this.state.language] }
              isChecked={ this.state.agreement } 
              onChangeCheck={ (checked) => { this.setState({agreement:!checked}) } }
              error={this.state.errors.agreement} />
            
          </View>

          <TouchableOpacity activeOpacity={0.8} onPress={ () => this.nextStep() } >
            <Text style={ mystyles.formButton }>{ language.register.form.button[this.state.language] }</Text>
          </TouchableOpacity>
        </ScrollView>

      </KeyboardAvoidingView>
    );
  }

}