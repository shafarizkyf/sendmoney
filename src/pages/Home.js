import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  Navbar,
  TopMenu, 
  Footer 
} from '../components';
import colors from '../config/colors';
import language from '../config/languages';

export default class Home extends Component {
  
  state = {
    language: 'id',
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  render(){
    return(
      <View style={ styles.page }>
        <Navbar openDrawer={ () => this.props.navigation.openDrawer() } />
        <TopMenu 
          homePage={ () => this.props.navigation.navigate('home') }
          servicePage={ () => this.props.navigation.navigate('service') } 
          featurePage={ () => this.props.navigation.navigate('feature') }
          pricingPage={ () => this.props.navigation.navigate('pricing') }
          language={ this.state.language } selected={ 1 } />
          
        <ScrollView>
          <View style={ styles.banner }>
            <Image source={ require('../../assets/img/banner.png') } style={ styles.bannerImage } />
          </View>

          <View style={{ flex:1, }}>
            <View style={{ paddingVertical:35, paddingHorizontal:30 }}>
              <Text style={ styles.homeDesc }>{ language.home.content.descTop[this.state.language] }</Text>
              <TouchableOpacity onPress={ () => { this.props.navigation.navigate('biodata') } }>
                <Text style={ styles.registerBtn }>{ language.home.content.buttonRegister[this.state.language] }</Text>
              </TouchableOpacity>
            </View>

            <View style={{ backgroundColor: colors.grey }}>
              <View style={ styles.whyUs }>
                <Image style={ styles.whyUsIcon } source={ require('../../assets/img/mudah.png') } />
                <Text style={ styles.whyUsText }>{ language.home.content.whyUs1[this.state.language] }</Text>
              </View>
              <View style={ styles.whyUs }>
                <Image style={ styles.whyUsIcon } source={ require('../../assets/img/aman.png') } />
                <Text style={ styles.whyUsText }>{ language.home.content.whyUs2[this.state.language] }</Text>
              </View>
              <View style={ styles.whyUs }>
                <Image style={ styles.whyUsIcon } source={ require('../../assets/img/terpercaya.png') } />
                <Text style={ styles.whyUsText }>{ language.home.content.whyUs3[this.state.language] }</Text>
              </View>
            </View>

            <View style={{ paddingVertical:35, paddingHorizontal:30 }}>
              <Text style={ styles.homeDesc }>{ language.home.content.descBottom[this.state.language] }</Text>
            </View>

            <Footer />
          </View>
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  page:{
    flex:1
  },
  banner:{
    justifyContent: 'center',
    alignItems: 'center',
  },
  bannerImage:{
    height: 300,
    aspectRatio: 1.6,
    resizeMode: 'contain'  
  },
  homeDesc:{
    color: colors.black,
    textAlign: 'center',
    fontSize: 16,
  },
  registerBtn:{
    width: '50%',
    alignSelf: 'center',
    backgroundColor: colors.orange,
    color: 'white',
    textAlign: 'center',
    borderRadius: 3,
    paddingVertical: 10,
    marginTop: 10
  },
  whyUs:{
    backgroundColor: colors.grey,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  whyUsIcon:{
    flex:1,
    width:100,
    height:100,
    resizeMode: 'center',
  },
  whyUsText:{
    color: colors.navy,
    fontSize: 18
  }
})