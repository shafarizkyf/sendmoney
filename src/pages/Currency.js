import React from 'react';
import { 
  View,
  ScrollView
} from 'react-native';
import {
  Navbar,
  Breadcrumb,
  TableHead,
  TableBody,
} from '../components';
import mystyles from '../styles/mystyles';
import Axios from 'axios';
import api from '../config/api';
import languages from '../config/languages';

export default class Currency extends React.Component {
  
  state = {
    language: 'id',
    tableHeader: [
      languages.page.currency.tableHeader.code[this.props.screenProps.selectedLanguage], 
      languages.page.currency.tableHeader.currency[this.props.screenProps.selectedLanguage], 
      languages.page.currency.tableHeader.status[this.props.screenProps.selectedLanguage]
    ],
    tableBody: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage});
    this.userCurrency();
  }

  userCurrency(){
    const data = {
      com: api.com.getUserCurrency,
      userId: this.props.screenProps.user.id,
      password: this.props.screenProps.user.password,
    };

    console.log('user currency -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('user currency -->', response);

      let userCurrency = [];
      if(response.resultCode == api.response.success){
        userCurrency = response.data.map(obj => {
          return [
            obj.kode_mata_uang,
            obj.nama_mata_uang,
            obj.status ? 'Aktif' : 'Tidak Aktif'
          ]
        });

        this.setState({tableBody: userCurrency});
      }

    }).catch(error => {
      console.log('error user currency -->', error);
    });
  }

  render(){
    return (
      <View style={[mystyles.page]}>
        <Navbar 
          showNotificationIcon={ this.props.screenProps.isLoggedIn }
          openDrawer={ () => this.props.navigation.openDrawer() } 
          notification={ () => this.props.navigation.navigate('notification') }/>
          
        <Breadcrumb 
          title={ languages.menu.currency[this.state.language] } 
          onBack={ () => this.props.navigation.goBack() }/>

        <View style={[mystyles.mt10]}></View>
        <ScrollView>          
          <View>
            <TableHead data={this.state.tableHeader} />
            {
              this.state.tableBody.map((obj, index) => <TableBody key={index} data={obj} index={index} />)
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}