import React, { Component } from 'react';
import { 
  View, 
  Text,
  ImageBackground,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import {
  Navbar,
  TopMenu,
  Label,
} from '../components';
import language from '../config/languages';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';
import Axios from 'axios';
import api from '../config/api';
import validate from 'validate.js';
import loginValidation from '../validation/LoginValidation';

export default class Login extends Component {

  state = {
    language: 'id',
    errors: []
  }

  componentDidMount(){
    this.setState({language: this.props.screenProps.selectedLanguage})
  }

  login(){
    let data = {
      com: api.com.login,
      email: this.state.email,
      password: this.state.password
    };

    let errors = validate(this.state, loginValidation);
    console.log('login validation -->', errors);

    if(errors){
      this.setState({errors});
    }else{
      this.setState({errors:[]});

      Axios.post(api.baseUrl, data).then(response => {
        response = response.data;
        let hasPin = response.resultCode  == api.response.verification;
        if(response.resultCode == api.response.success || hasPin){
          
          let userData = {
            email: this.state.email,
            password: this.state.password,
            id: response.data[0].userId,
            hasPin: hasPin,
          }

          this.props.screenProps.hasLoggedIn();
          this.props.screenProps.setUser(userData);
          
          this.setupBasicApi();
          
        }else if(response.resultCode == api.response.error){
          this.setState({error: 'Kombinasi Email dan Password tidak sesuai'});
        
        }else if(response.resultCode == api.response.empty){
          this.setState({error: 'User tidak ditemukan'});
        }

      }).catch(error => {
        this.setState({error: 'Internal Server Error'});
        console.log('error login -->', error);
      });
    }
  }

  async setupBasicApi(){
    await this.props.screenProps.getBalance();
    await this.props.screenProps.getBanks();
    await this.props.screenProps.getCurrency();
    await this.props.screenProps.getUserBanks();
    await this.props.screenProps.getUserCurrency();

    this.props.navigation.navigate('menu');
  }

  render(){
    return(
      <View style={ mystyles.page }>
        <Navbar
          showNotificationIcon={false}
          openDrawer={ () => this.props.navigation.openDrawer() } />

        <TopMenu 
          homePage={ () => { this.props.navigation.navigate('home') } }
          featurePage={ () => { this.props.navigation.navigate('feature') } }
          pricingPage={ () => { this.props.navigation.navigate('pricing') } }
          language={ this.state.language } selected={ 0 } />

        <View style={ mystyles.page }>
          <ImageBackground source={ require('../../assets/img/background.png') } style={ mystyles.loginBg } />
          
          { this.state.error && <Label isError={true} message={this.state.error} /> }

          <View style={ styles.loginForm }>
            <Text style={ styles.title }>Login</Text>
            
            <TextInput 
              style={ styles.textInput }
              underlineColorAndroid={ 'rgba(0,0,0,0)' } 
              onChangeText={ (email) => this.setState({email}) }
              value={ this.state.email }
              placeholder="E-mail"
              autoCapitalize="none" />
            {
              this.state.errors.email &&
              <Text style={[mystyles.f12, mystyles.textRed, mystyles.mb10]}>{this.state.errors.email}</Text>
            }

            <TextInput 
              style={ styles.textInput }
              secureTextEntry={true}
              underlineColorAndroid={ 'rgba(0,0,0,0)' }
              onChangeText={ (password) => this.setState({password}) }
              value={ this.state.password }
              placeholder="Password"
              autoCapitalize="none" />
            {
              this.state.errors.password &&
              <Text style={[mystyles.f12, mystyles.textRed, mystyles.mb10]}>{this.state.errors.password}</Text>
            }

            <TouchableOpacity onPress={ () =>  this.login() }>
              <Text style={ styles.loginBtn } >Login</Text>
            </TouchableOpacity>
            
            <View style={[mystyles.row, mystyles.spaceBetween, mystyles.mt20] }>
              <TouchableOpacity onPress={ () => { this.props.navigation.navigate('register', {updateData:false}) } }>
                <Text style={[ mystyles.textGrey, mystyles.f10]}>
                  { language.login.form.registerText[this.state.language] } 
                  <Text style={[ mystyles.bold, mystyles.textTeal ]}>
                  { ' ' + language.login.form.register[this.state.language].toUpperCase() } 
                  </Text>
                </Text>
              </TouchableOpacity>
              
              <TouchableOpacity onPress={ () => this.props.navigation.navigate('forgetPassword') } >
                <Text style={[ mystyles.textGrey, mystyles.f10, mystyles.italic]}>
                  { language.login.form.forgetPassword[this.state.language] }
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loginForm:{
    marginTop: 50,
    marginHorizontal: 40,
    paddingHorizontal: 30,
    paddingVertical: 30,
    borderRadius: 10,
    backgroundColor: 'rgba(51, 111, 161, 0.9)',
    elevation: 10,
  },
  title:{
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
  },
  textInput:{
    paddingVertical:5,
    paddingHorizontal:10,
    marginBottom: 10,
    borderRadius: 3,
    color: colors.black,
    backgroundColor: '#cfd8de',
  },
  loginBtn:{
    padding: 10,
    backgroundColor: colors.orange,
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    borderRadius: 3,
  }
});