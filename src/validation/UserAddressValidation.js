const validation = {
  address:{
    presence: true
  },
  city:{
    presence: true
  },
  province:{
    presence: true
  },
  zipcode:{
    presence: true
  },
  country:{
    presence: true
  },
}

export default validation;