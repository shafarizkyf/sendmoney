const validation = {
  type:{
    presence: true
  },
  number:{
    presence: true
  },
  expire:{
    presence: true
  }
}

export default validation;