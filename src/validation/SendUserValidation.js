const validation = {
  currency:{
    presence:true,
  },
  from:{
    presence:true,
  },
  to:{
    presence:true,
  },
  amount:{
    presence:true,
  }
}

export default validation;