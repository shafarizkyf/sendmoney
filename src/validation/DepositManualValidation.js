const validation = {
  currency:{
    presence:true,
  },
  method:{
    presence:true,
  },
  amount:{
    presence:true,
  },
  note:{
    presence:true
  }
}

export default validation;