const validation = {
  name:{
    presence: true,
  },
  date:{
    presence: true,
  },
  email:{
    presence: true,
    email: true
  },
  phone:{
    presence: true,
    length:{
      minimum: 11,
      maximum: 13 
    }
  },
}

export default validation;