const validation = {
  bank:{
    presence: true,
  },
  currency:{
    presence: true
  },
  number:{
    presence: true
  },
  name:{
    presence: true
  }
}

export default validation;