const validation = {
  currency:{
    presence:true,
  },
  bank:{
    presence:true,
  },
  number:{
    presence:true,
  },
  amount:{
    presence:true,
  }
}

export default validation;