const validation = {
  currentPassword:{
    presence: true
  },
  newPassword:{
    presence: true,
    length:{
      minimum:6,
      maximum:30
    }
  },
  confirmPassword:{
    presence: true,
    equality: 'newPassword'
  },
}

export default validation;