const validation = {
  phone:{
    presence: true
  },
  otp:{
    presence: true
  }
}

export default validation;