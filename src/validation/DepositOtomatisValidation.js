const validation = {
  currency:{
    presence:true,
  },
  bank:{
    presence:true,
  },
  amount:{
    presence:true,
  },
  note:{
    presence:true
  }
}

export default validation;