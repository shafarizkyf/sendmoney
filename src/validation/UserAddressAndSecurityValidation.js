const validation = {
  password:{
    presence: true,
    length: {
      minimum: 6,
      maximum: 30
    }
  },
  confirmPassword:{
    presence: true,
    equality: 'password'
  },
  securtyQuestion:{
    presence: true,
  },
  securtyAnswer:{
    presence: true
  },
  address:{
    presence: true
  },
  city:{
    presence: true
  },
  province:{
    presence: true
  },
  zipcode:{
    presence: true,
    numericality: true,
  },
  country:{
    presence: true
  },
  agreement: {
    presence: {
      message: "^You need to check the checkbox"
    },
    inclusion: {
      within: [true],
      message: "^You need to check the checkbox"
    }
  }
}

export default validation;