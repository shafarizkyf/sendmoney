const validation = {
  currency:{
    presence:true,
  },
  bank:{
    presence:true,
  },
  number:{
    presence:true,
  },
  from:{
    presence:true,
  },
  amount:{
    presence:true,
  }
}

export default validation;