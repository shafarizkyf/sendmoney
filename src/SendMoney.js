import React, { Component } from 'react';
import { View, AsyncStorage} from 'react-native';
import MainNavigation from './navigation/MainNavigation';
import Axios from 'axios';
import api from './config/api';
import currencies from './config/currencies';

console.disableYellowBox = true;
export default class SendMoney extends Component{

  state = {
    isLoggedIn: false,
    language: 'id',
    user: [],
    balance: []
  }

  async componentDidMount(){
    const language = await AsyncStorage.getItem('language');
    if(language){
      this.setState({language});
    }
  }

  _getBalance(){
    const data = {
      com: api.com.balance,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    console.log('balance -->', data);

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('balance -->', response);

      let balance = [{label:'', value:'0'}];
      if(response.resultCode == api.response.success){
        balance = response.data.map(obj => {
          return {
            label: `${obj.mataUang} - ${obj.jumlah}`,
            value: `${obj.jumlah}`,
          }
        });
      }

      this.setState({balance});

    }).catch(error => {
      console.log('error banks -->', error);
    })
  }

  _getBanks(code = ''){
    const data = {
      com: api.com.getBank,
      kodeBank: code,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('banks -->', response);

      let banks = [];
      if(response.resultCode == api.response.success){
        banks = response.data.map(obj => {
          return {
            label: obj.nama,
            value: obj.kode,
            jenis: obj.jenis
          }
        });
      }

      this.setState({banks});

    }).catch(error => {
      console.log('error banks -->', error);
    })
  }

  _getUserBanks(){
    const data = {
      com: api.com.getUserBank,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('user banks -->', response);

      let userBanks = [];
      if(response.resultCode == api.response.success){
        userBanks = response.data.map(obj => {
          return {
            label: obj.nama_nasabah,
            value: obj.nomor_rekening,
            code: obj.kode_bank,
            currency: obj.mata_uang,
            id: obj.idBankUser
          }
        });
      }

      this.setState({userBanks});

    }).catch(error => {
      console.log('error user banks -->', error);
    });
  }

  _getCurrency(code = ''){
    const data = {
      com: api.com.getCurrency,
      kodeMataUang: code,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('currency -->', response);

      let currency = [];
      if(response.resultCode == api.response.success){
        currency = response.data.map(obj => {
          return {
            label: obj.namaMataUang,
            value: obj.kodeMataUang,
            flag: api.flagApi.replace('{country}', currencies[obj.kodeMataUang]),
          }
        });

        this.setState({currency});
      }

    }).catch(error => {
      console.log('error currency -->', error);
    });
  }

  _getUserCurrency(){
    const data = {
      com: api.com.getUserCurrency,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    Axios.post(api.baseUrl, data).then(response => {
      response = response.data;
      console.log('user currency -->', response);

      let userCurrency = [];
      if(response.resultCode == api.response.success){
        userCurrency = response.data.map(obj => {
          return {
            label: obj.namaMataUang,
            value: obj.kodeMataUang,
            flag: api.flagApi.replace('{country}', currencies[obj.kodeMataUang]),
          }
        });
      }

      this.setState({userCurrency});

    }).catch(error => {
      console.log('error user currency -->', error);
    });
  }

  _getDepositMethod(){

    const data = {
      com: api.com.depositMethod,
      userId: this.state.user.id,
      password: this.state.user.password
    };

    Axios.post(api.baseUrl, data)
    .then(response => {
      response = response.data;
      console.log('get deposit method -->', response);

      let depositMethod = [];
      if(response.resultCode == api.response.success){
        depositMethod = response.data.map(obj => {
          return {
            value: obj.id,
            label: obj.namaMetode
          }
        })
      }

      this.setState({depositMethod});

    }).catch(error => {
      console.log('error get deposit method -->', error);
    });
  }

  async _onLanguageChange(val){
    await AsyncStorage.setItem('language', val);
    this.setState({language:val});
  }

  _setUser(val){
    this.setState({user:val});
  }

  render(){
    return(
      <View style={{flex:1}}>
        <MainNavigation 
          screenProps={
            {
              isLoggedIn: this.state.isLoggedIn,
              selectedLanguage: this.state.language,
              onLanguageChange: (val) => this._onLanguageChange(val),
              setUser: (val) => this._setUser(val),
              getBanks: (code) => this._getBanks(code),
              getBalance: () => this._getBalance(),
              getUserBanks: () => this._getUserBanks(),
              getCurrency: (code) => this._getCurrency(code),
              getUserCurrency: (code) => this._getUserCurrency(),
              getDepositMethod: () => this._getDepositMethod(),
              hasLoggedIn: () => this.setState({isLoggedIn:true}),
              balance: this.state.balance,
              banks: this.state.banks,
              currency: this.state.currency,
              userBanks: this.state.userBanks,
              userCurrency: this.state.userCurrency,
              depositMethod: this.state.depositMethod,
              user: this.state.user
            }
          }/>
      </View>
    );
  }

}