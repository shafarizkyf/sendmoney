import React from 'react';
import { 
  View, 
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Modal,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const CardExchange = (props) => {

  return (
    <View style={[mystyles.mt20]}>
      <Modal 
        animationType="slide" 
        transparent={false} 
        visible={props.modalVisible}
        onRequestClose={()=>props.closeModal()}>

        <View style={[mystyles.page, mystyles.mt20, mystyles.mh20]}>
          {
            props.currencyOptions.map((obj, index) => (
              <TouchableOpacity 
                style={[styles.flagList]} 
                key={index} 
                onPress={()=>{props.onSelectCurrency(index)}}>
                <View style={[mystyles.row]}>
                    <Image source={ {uri:obj.icon} } style={[mystyles.iconFlag20]} />
                  <Text style={[mystyles.ml10, mystyles.f16]}>{obj.currency} ({obj.symbol})</Text>
                </View>
              </TouchableOpacity>
            ))
          }
        </View>
      </Modal>

      <View style={[styles.cardHeader]}>
        <TouchableOpacity onPress={ () => { props.onChangeExchange(props.index) } }>
          <View style={[mystyles.row]}>
            <Image source={{uri:props.moneyIcon}} style={[mystyles.iconFlag20]} />
            <Text style={[ mystyles.ml10 ]}>{ props.moneyText }</Text>
            <Text style={[mystyles.f10, mystyles.ml10, mystyles.textDarkGrey]}>{props.status}</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={[styles.cardBody]}>
        <TextInput
          placeholder={ `${props.symbol} 0` }
          style={[mystyles.textInput]}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          keyboardType="numeric"
          onChangeText={(val) => props.onChangeText(props.index, val)}
          value={ props.value  }
          editable={ props.index == 1 ? false : true }
          selectTextOnFocus={ props.index == 1 ? false : true }
        />
        {/* <Text style={[mystyles.mt10, mystyles.textDarkGrey]}>{ props.description }</Text> */}
        {
          props.errors &&
          <Text style={[mystyles.textRed, mystyles.f12]}>{ props.index == 0 ? props.errors.from : props.errors.to }</Text>
        }
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cardHeader:{
    backgroundColor: colors.yellow,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  cardBody:{
    padding: 20,
    borderWidth: 1,
    borderColor: colors.grey
  },
  flagList:{
    borderBottomWidth: 1,
    borderBottomColor: colors.grey,
    padding: 10,
    marginBottom: 10,
    width: '100%'
  }
});

export default CardExchange;
