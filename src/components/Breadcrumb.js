import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const Breadcrumb = (props) => {
  return (
    <View style={ styles.breadcrumb } >
      <View style={[ mystyles.row ]}>
        <TouchableOpacity onPress={ props.onBack }>
          <Image source={ require('../../assets/img/back.png') } style={ styles.breadcrumbIcon } />
        </TouchableOpacity>
        <Text style={ styles.breadcrumbText }>{ props.title }</Text>
      </View>
    </View>
  );
}

export default Breadcrumb;

const styles = StyleSheet.create({
  breadcrumb:{
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: colors.navy,
    borderBottomWidth: 5,
    borderBottomColor: colors.orange
  },
  breadcrumbIcon:{
    height:20,
    width:20
  },
  breadcrumbText:{
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
    marginLeft: 10,
  }
});