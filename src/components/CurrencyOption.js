import React, { Component } from 'react';
import { View, Text, Picker, Image } from 'react-native';
import mystyles from '../styles/mystyles';
import colors from '../config/colors';

const CurrencyOption = (props) => {
  return(
    <View>
      <View style={[mystyles.row]}>
        <View style={[mystyles.row, mystyles.selfCenter]}>
          <Text style={[mystyles.bold, mystyles.bold, mystyles.f14]}>{props.text}</Text>
          <Image source={ {uri:props.flag} } style={[mystyles.iconFlag20, mystyles.ml10]}  />
        </View>
        <View style={[mystyles.page, {alignItems:'flex-end'}, mystyles.ml20]}>
          <View style={[mystyles.formGroup, {width:'100%',}]}>
            <View style={[mystyles.border, {borderColor: '#c9c9c9'}]}>
              <Picker 
                style={{ height: 30, backgroundColor:'#f7f7f7' }} 
                onValueChange={(val, position) => { props.onChange(val, position) }}
                selectedValue={ props.value } >
                { props.items.map(obj => <Picker.Item label={obj.label} value={obj.value} key={obj.value} color={ colors.black }/>) }                
              </Picker>
            </View>
          </View>
        </View>
      </View>
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12, mystyles.textRight]}>{props.error}</Text>
      }
    </View>
  );
}

export default CurrencyOption;