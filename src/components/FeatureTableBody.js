import React from 'react';
import { View, Text, Image } from 'react-native';
import mystyles from '../styles/mystyles';

const FeatureTableBody = (props) => {
  return (
    <View style={[mystyles.row]}>
      {
        props.data.map((obj, index) => {
          let width = (100/props.data.length).toFixed(2) + '%';
          return typeof obj === 'string'
          ? <Text 
            key={index} 
            style={[
              index == 0 ? mystyles.feature : mystyles.textCenter,
              props.index%2 == 0 && index != 0 ? mystyles.bgGrey : mystyles.bgWhite,  
              mystyles.f10,
              {width, textAlignVertical:'center'}
            ]}>{obj}</Text>
          : <View key={index} style={[props.index%2 == 0 ? mystyles.bgGrey : mystyles.bgWhite, mystyles.justifyCenter, {width}]}>
            <Image 
              source={ obj ? require('../../assets/img/mark.png') : require('../../assets/img/silang.png') } 
              style={[ obj ? mystyles.checkMark20 : mystyles.crossMark15 , mystyles.selfCenter]} />
            </View>
        })        
      }
    </View>
  );
}

export default FeatureTableBody;