import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
} from 'react-native';
import mystyles from '../styles/mystyles';


const MyImageInput = (props) => {
  return (
    <View style={mystyles.formGroup}>
      <Text style={mystyles.formLabel}>{props.label} {props.isRequiredMark ? '*' : null}</Text>
      <View style={[mystyles.border]}>
        <TouchableOpacity activeOpacity={0.8} onPress={ () => props.onSelectImage() }>
            <Text style={[mystyles.pv10, mystyles.ph20]}>Browse</Text>
        </TouchableOpacity>
      </View>
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12]}>{props.error}</Text>
      }
    </View>
  );
}

export default MyImageInput;