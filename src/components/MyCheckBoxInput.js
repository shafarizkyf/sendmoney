import React from 'react';
import { View, Text } from 'react-native';
import CheckBox from 'react-native-checkbox';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const MyCheckBoxInput = (props) => {
  return (
    <View style={ mystyles.formGroup }>
      <View style={{ flexDirection:'row' }}>
        <CheckBox
          checkboxStyle={{ width:15, height:15, marginRight:5, marginTop:5 }}
          label={''}
          checked={ props.isChecked }
          onChange={(checked) => props.onChangeCheck(checked) }
        />
        <Text style={[mystyles.f12, {width:'80%'}]}>
          { props.label }
        </Text>
      </View>
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12]}>{props.error}</Text>
      }
    </View>  
  );
}

export default MyCheckBoxInput;