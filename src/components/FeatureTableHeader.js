import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import mystyles from '../styles/mystyles';

const FeatureTableBody = (props) => {
  return (
    <View style={[mystyles.row]}>
      {
        props.data.map((obj, index) => {
          let width = (100/props.data.length).toFixed(2) + '%';
          return <Text 
            key={index}
            style={[mystyles.featureHeader, index == 0 ? mystyles.bgLightYellow : mystyles.bgYellow, {width, textAlign:'center'}]}>
          {obj}</Text>
        })
      }
    </View>
  );
}

export default FeatureTableBody;