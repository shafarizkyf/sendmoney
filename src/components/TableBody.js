import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import language from '../config/languages';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const TableBody = (props) => {
  return(
    <View style={[mystyles.row, styles.tableBody]}>
      {
        props.data.map((obj, index) => {
          let width = (100 / props.data.length).toFixed(2);
          width += '%'
          return <Text key={index} style={[styles.tableBodyText, (props.index%2 == 0) ? mystyles.bgYellow : mystyles.bgGrey, {width:width}]}>{ obj }</Text>
        })
      }
    </View>
  );
}

const styles = StyleSheet.create({
  tableBodyText:{
    fontSize: 12,
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: colors.black,
  },
})

export default TableBody;