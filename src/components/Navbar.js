import React from 'react';
import {
  View, 
  Text, 
  Image,
  TouchableOpacity, 
  StyleSheet,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const Navbar = (props) => {

  return (
    <View style={{ height:50, backgroundColor:'white' }}>
      <View style={ styles.navRow }>
        <TouchableOpacity onPress={props.openDrawer}>
          <Image source={ require('../../assets/img/menu.png') } style={[mystyles.icon20, mystyles.mr20]} />
        </TouchableOpacity>
        <Image source={ require('../../assets/img/logo.png') } style={{width:108, height:20}} />
        {
          props.showNotificationIcon &&
          <TouchableOpacity onPress={props.notification} style={styles.notification}>
            <Image source={ require('../../assets/img/notifikasi.png') } style={mystyles.icon20} />
          </TouchableOpacity>
        }
      </View>
    </View>
  )
}

export default Navbar;

const styles = StyleSheet.create({
  navRow:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },
  notification:{
    flex:1,
    alignItems:'flex-end', 
  }
});