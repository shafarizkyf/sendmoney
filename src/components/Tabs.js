import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const Tabs = (props) => {
  return (
    <View style={[mystyles.row]}>
      { 
        props.tabs.map((tab, index) => {
          return <Text 
            style={[
              styles.tab, props.selected == tab.tabId ? mystyles.bgOrange : {}, 
              (props.tabs.length % 2 !== 0 && index % 2 === 0) ? styles.borderCenter : {}]} 
            onPress={() => props.navigation(tab.tabId) }
            key={index}>{ tab.label }</Text>
        }) 
      }      
    </View>
  )
}

const styles = StyleSheet.create({
  tab: {
    backgroundColor: colors.lightOrange,
    flex: 1,
    textAlign: 'center',
    color: 'white',
    paddingVertical: 10,
  },
  borderCenter: {
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderRightColor: 'white',
    borderLeftColor: 'white',
  }
});

export default Tabs;