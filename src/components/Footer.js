import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import language from '../config/languages';
import colors from '../config/colors';

const Footer = () => {
  return(
    <View style={{ padding:10, backgroundColor: colors.black }}>
      <Text style={[styles.footerText]}>Copyright © 2018 PT SendMoney </Text>
      <Text style={[styles.footerText]}>All rights reserved</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  footerText:{
    textAlign:'center', 
    color: colors.grey,
    fontSize: 10
  }
})

export default Footer;