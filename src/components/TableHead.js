import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import language from '../config/languages';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const TableHead = (props) => {
  return(
    <View style={[mystyles.row, styles.tableHead]}>
      {
        props.data.map((obj, index) => {
          let width = (100 / props.data.length).toFixed(2);
          width += '%';
          return <Text key={index} style={[styles.tableHeadText, {width:width}]}>{obj}</Text>
        })
      }
    </View>
  );
}

const styles = StyleSheet.create({
  tableHead:{
    backgroundColor: colors.lightBlue,
  },
  tableHeadText:{
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 5,
    paddingHorizontal: 10,
    fontSize: 12,
    borderBottomWidth: 2,
    borderBottomColor: colors.navy
  }
})

export default TableHead;