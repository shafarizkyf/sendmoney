import React from 'react';
import { Text } from 'react-native';
import mystyles from '../styles/mystyles';

const Label = (props) => (
  <Text style={[props.isError ? mystyles.bgRed : mystyles.bgGreen, mystyles.textWhite, mystyles.p5, mystyles.textCenter]}>
    { props.message }
  </Text>              
);

export default Label;