import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import language from '../config/languages';
import colors from '../config/colors';

const TopMenu = (props) => {
  return (
    <View style={ styles.navbar }>
      <Text 
        onPress={ props.homePage } 
        style={ props.selected == 1 ? styles.navbarMenuSelected : styles.navbarMenu }>
        { language.home.navbarMenu.home[props.language].toUpperCase() }
      </Text>
      <Text 
        onPress={ props.servicePage } 
        style={ props.selected == 2 ? styles.navbarMenuSelected : styles.navbarMenu }>
        { language.home.navbarMenu.service[props.language].toUpperCase() }
      </Text>
      <Text 
        onPress={ props.featurePage }
        style={ props.selected == 3 ? styles.navbarMenuSelected : styles.navbarMenu }>
        { language.home.navbarMenu.feature[props.language].toUpperCase() }
      </Text>
      <Text 
        onPress={ props.pricingPage }
        style={ props.selected == 4 ? styles.navbarMenuSelected : styles.navbarMenu }>
        { language.home.navbarMenu.pricing[props.language].toUpperCase() }
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  navbar:{
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    backgroundColor: colors.navy,
  },
  navbarMenu:{
    marginHorizontal: 10,
    paddingHorizontal: 5,
    paddingTop: 20,
    paddingBottom: 10,
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
  },
  navbarMenuSelected:{
    marginHorizontal: 10,
    paddingHorizontal: 5,
    paddingTop: 20,
    paddingBottom: 6,
    fontSize: 12,
    fontWeight: 'bold',
    color: 'white',
    borderBottomColor: colors.orange,
    borderBottomWidth: 4
  },
});

export default TopMenu;