import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import mystyles from '../styles/mystyles';

const MenuButton = (props) => {
  return (
      <TouchableOpacity activeOpacity={0.7} onPress={ props.navigation  } >
        <Text style={ mystyles.menu }>{ props.label }</Text>
      </TouchableOpacity>
  );
}

export default MenuButton;