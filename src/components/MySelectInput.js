import React from 'react';
import { View, Text, Picker } from 'react-native';
import mystyles from '../styles/mystyles';

const MySelectInput = (props) => {
  return (
    <View style={mystyles.formGroup}>
      <Text style={mystyles.formLabel}>{props.label} {props.isRequiredMark ? '*' : null}</Text>
      <View style={[mystyles.border]}>
        <Picker style={{ height: 41 }} onValueChange={ (val) => { props.onChangeSelect(val) } } selectedValue={ props.value } >
          { props.items.map((obj, index) => <Picker.Item key={index} label={obj.label} value={obj.value} /> ) }
        </Picker>
      </View>
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12]}>{props.error}</Text>
      }
    </View>
  );
}

export default MySelectInput;