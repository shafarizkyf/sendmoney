import React from 'react';
import { View, Text, TextInput } from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const MyTextInput = (props) => {
  return (
    <View style={[mystyles.formGroup, props.inputWrapperStyle ]}>
      <Text style={ mystyles.formLabel } >
        { props.label } { props.isRequiredMark ? '*' : '' }
      </Text>
      <TextInput
        value={ props.value }
        onChangeText={ (val) => props.onChangeText(val) } 
        placeholder={ props.label }
        placeholderTextColor={ colors.grey }
        underlineColorAndroid={ 'rgba(0,0,0,0)' }
        secureTextEntry={ props.isPasswordField ? true : false }
        style={ mystyles.textInput }
        keyboardType={ props.keyboardType ? props.keyboardType : 'default' }
        multiline={ props.multiline }
        editable={ !props.disabled }
        selectTextOnFocus={ !props.disabled } />
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12]}>{props.error}</Text>
      }
    </View>
  );
}

export default MyTextInput;