import React from 'react';
import { View, Text } from 'react-native';
import DatePicker from 'react-native-datepicker'
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

const MyDateInput = (props) => {
  return (
    <View style={ mystyles.formGroup }>
      <Text style={ mystyles.formLabel } >
        { props.label } { props.isRequiredMark ? '*' : '' }
      </Text>
      <DatePicker
        date={ props.value }
        onDateChange={ (val) => props.onChangeDate(val) }
        placeholder={ props.label }
        showIcon={ false }
        style={{ width:'100%' }}
        customStyles={{
          dateInput:{
            alignItems: 'flex-start',
            paddingHorizontal: 20,
            borderColor: colors.black
          },
          placeholderText:{
            color: colors.grey
          }
        }}
      />
      {
        props.error &&
        <Text style={[mystyles.textRed, mystyles.f12]}>{props.error}</Text>
      }
    </View> 
  );
}

export default MyDateInput;