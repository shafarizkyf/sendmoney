import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet,
  Picker,
  Image,
} from 'react-native';
import colors from '../config/colors';
import mystyles from '../styles/mystyles';

export default class Balance extends React.Component{

  componentDidMount(){
    console.log('balance dropdown -->', this.props);
  }

  render(){
    return(
      <View style={[ mystyles.row, mystyles.mh20 ]}>
        <Image source={ require('../../assets/img/saldo.png') } style={[mystyles.icon50]} />
        <View style={[ mystyles.column, mystyles.mt5, mystyles.ml10 ]}>
          <Text style={[mystyles.textNavi, mystyles.f14]}>Saldo</Text>
          <Text style={[mystyles.textOrange, mystyles.bold, mystyles.f14]}>SendMoney</Text>
        </View>
        <View style={[mystyles.page, {alignItems:'flex-end'}, mystyles.ml20]}>
          <View style={[mystyles.formGroup, {width:'100%',}]}>
            <View style={[mystyles.border, {borderColor: '#c9c9c9'}]}>
              <Picker style={{ height: 30, backgroundColor:'#f7f7f7' }} >
                {
                  this.props.data.map((obj,index) => <Picker.Item key={index} label={obj.label} value={obj.value} color={ colors.black }/>)
                }
              </Picker>
            </View>
          </View>
        </View>
      </View>
    );
  }
}