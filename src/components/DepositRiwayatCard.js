import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import mystyles from '../styles/mystyles';

const DepositRiwayatCard = (props) => {
  return(
    <View style={[]}>
      <View style={[styles.card, mystyles.mh20]}>
        <View style={[mystyles.row, mystyles.spaceBetween]}>
          <View style={[styles.left]}>
            <Text style={[mystyles.f10, mystyles.mb5, mystyles.textNavi]}>{ props.data.date }</Text>
            <Text style={[mystyles.f10, mystyles.bold]}>{ props.data.transactionType }</Text>
            <Text style={[mystyles.f10]}>{ props.data.currency }</Text>
            <Text style={[mystyles.f10]}>{ props.data.account }</Text>
            <Text style={[mystyles.f12, mystyles.bold]}>{ props.data.amount }</Text>
          </View>
          <View style={[styles.right, mystyles.justifyCenter]}>
          {
            props.type == 'pending' 
            ? <View>
                <Text style={[mystyles.f10, mystyles.textRed, mystyles.bold]}>Batas Waktu Deposit</Text>
                <Text style={[mystyles.f12, mystyles.bold]}>{props.data.expire}</Text>
              </View>

            : props.type == 'success' 
            ? <Text style={[mystyles.f12, mystyles.bold, mystyles.textGreen]}>{props.status}</Text>
            : <Text style={[mystyles.f12, mystyles.bold, mystyles.textGreen]}>{props.status}</Text>
          }
          </View>
        </View>
        <TouchableOpacity 
          onPress={ props.detailPage }
          style={ props.detail ? mystyles.show : mystyles.hide }>
          <Text style={[mystyles.f12, mystyles.textRight, mystyles.textNavi]}>Lihat Detail ></Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: 'white',
    marginTop: 10,
    padding: 20,
    borderRadius: 3,
  },
});

export default DepositRiwayatCard;
