export default {
  black: "#333333",
  blue: '#5b9ed0',
  navy: "#336fa1",
  grey: "#f0f0f0",
  lightYellow: "#ffeabd",
  orange: "#ffb108",
  lightOrange: "#ffd67d",
  lightBlue: '#83b9e6',
  papaya: "#ff6600",
  yellow: "#ffda82",
  teal: "#0ac6cd",
  lightGreen: "#deedd0",
  green: "#3b7524",
  brown: "#805a21",
  red: "#e51b1b",
  moss: '#63dd16'
}