export default {
  drawer:{
    "login":{
      "en": "Login",
      "id": "Login"
    },
    "register":{
      "en": "Register",
      "id": "Daftar"
    },
    "logout":{
      "en": "Logout",
      "id": "Logout"
    },
    "termsAndCondition":{
      "en": "Terms and Conditions",
      "id": "Syarat dan Ketentuan"
    },
    "privacyPolicy":{
      "en": "Privacy Policy",
      "id": "Kebijakan Privasi"
    },
    "aboutUs":{
      "en": "About Us",
      "id": "Tentang Kami"
    },
    "language":{
      "en": "English",
      "id": "Bahasa"
    }
  },
  home: {
    navbarMenu: {
      "home": {
        "en": "Home",
        "id": "Beranda"
      },
      "service": {
        "en": "Service",
        "id": "Layanan"
      },
      "feature": {
        "en": "Feature",
        "id": "Fitur"
      },
      "pricing": {
        "en": "Pricing",
        "id": "Biaya"
      },
    },
    content:{
      "buttonRegister":{
        "en": "Register Now",
        "id": "Daftar Sekarang"
      },
      "descTop": {
        "en": "SendMoney is an App That Capable Transfer Your Money Within Minute",
        "id": "SendMoney Adalah Aplikasi Transfer Uang Dalam Hitungan Menit"
      },
      "descBottom": {
        "en": "Easy Domestic or Intenational Money Transfer",
        "id": "Transfer Uang Dengan Mudah Dalam dan Luar Negeri Kapanpun, Dimanapun, dan Tanpa Ribet"
      },
      "whyUs1":{
        "en": "Easy",
        "id": "Mudah"
      },
      "whyUs2":{
        "en": "Quick And Secure",
        "id": "Aman dan Cepat"
      },
      "whyUs3":{
        "en": "Trusted",
        "id": "Terpercaya"
      },
    }
  },
  login:{
    form:{
      "registerText":{
        "en": "Don't have account yet?",
        "id": "Belum memiliki akun?"
      },
      "register":{
        "en": "Register",
        "id": "Daftar"
      },
      "forgetPassword":{
        "en": "Forget Password",
        "id": "Lupa Password"
      }
    }
  },
  register:{
    step1:{
      "title":{
        "en": "Step 1: Basic Identity",
        "id": "Langkah 1: Data Diri"
      }
    },
    step2:{
      "title":{
        "en": "Step 2: Verification",
        "id": "Langkah 2: Verifikasi"
      }
    },
    step3:{
      "title":{
        "en": "Step 3: Address and Securiy",
        "id": "Langkah 3: Keamanan dan Alamat"
      }
    },
    form:{
      "name":{
        "en": "First and Last Name",
        "id": "Nama Lengkap"
      },
      "date":{
        "en": "Birthday",
        "id": "Tanggal Lahir"
      },
      "email":{
        "en": "Email",
        "id": "Email"
      },
      "phone":{
        "en": "Phone Number",
        "id": "No. Handphone"
      },
      "password":{
        "en": "Password",
        "id": "Password"
      },
      "confirmPassword":{
        "en": "Confirm Password",
        "id": "Konfirmasi Password"
      },
      "securtySection":{
        "en": "Security Information",
        "id": "Informasi Keamanan"
      },
      "securtyQuestion":{
        "en": "Secutiry Question",
        "id": "Pertanyaan Keamanan"
      },
      "securtyAnswer":{
        "en": "Answer",
        "id": "Jawaban"
      },
      "addressSection":{
        "en": "Address",
        "id": "Data Alamat"
      },
      "address":{
        "en": "Address",
        "id": "Alamat"
      },
      "city":{
        "en": "City",
        "id": "Kota"
      },
      "province":{
        "en": "Province",
        "id": "Provinsi"
      },
      "zipcode":{
        "en": "Zip Code",
        "id": "Kode Pos"
      },
      "country":{
        "en": "Country",
        "id": "Negara"
      },
      "agreement":{
        "en": "I have read and agree the privacy policy of SendMoney",
        "id": "Saya telah membaca dan setuju atas syarat dan ketentuan serta kebijakan privasi SendMoney"
      },
      "verification":{
        "en": "Verification Code",
        "id": "Kode Verifikasi"
      },
      "verificationAlert":{
        "en": "Please Check Your Email To Get",
        "id": "Silahkan Cek Email Anda Untuk Dapatkan"
      },
      "button":{
        "en": "Next",
        "id": "Lanjut"
      },
      "buttonUpdate":{
        "en": "Update",
        "id": "Perbarui"
      }
    }
  },
  page:{
    "deposit":{
      "tab":{
        "automatic":{
          "en":"Automatic",
          "id": "Otomatis"
        },
        "manual":{
          "en":"Manual",
          "id":"Manual"
        },
        "history":{
          "en":"History",
          "id":"Riwayat"
        }
      },
      "form":{
        "depositOtomatis":{
          "currency":{
            "en": "Currency",
            "id": "Mata uang"
          },
          "bank":{
            "en": "Choose bank",
            "id": "Pilih bank"
          },
          "amount":{
            "en": "Amount",
            "id": "Jumlah"
          },
          "note":{
            "en": "Note",
            "id": "Keterangan"
          },
          "proceed":{
            "en": "Proceed",
            "id": "Proses"
          }
        },
        "depositManual":{
          "currency":{
            "en": "Currency",
            "id": "Mata uang"
          },
          "depositMethod":{
            "en": "Choose deposit method",
            "id": "Pilih metode deposit"
          },
          "amount":{
            "en": "Amount",
            "id": "Jumlah"
          },
          "note":{
            "en": "Note",
            "id": "Keterangan"
          },
          "proceed":{
            "en": "Proceed",
            "id": "Proses"
          }
        }
      }
    },
    "send":{
      "tab":{
        "sendToUser":{
          "en": "User SendMoney",
          "id": "User SendMoney"
        },
        "sendViaBank":{
          "en": "Bank Transfer",
          "id": "Transfer Bank"
        },
        "history":{
          "en": "History",
          "id": "Riwayat"
        }
      },
      "form":{
        "sendUser":{
          "from":{
            "en": "From",
            "id": "Dari"
          },
          "to":{
            "en": "To",
            "id": "Kepada"
          },
          "currency":{
            "en": "Currency",
            "id": "Mata Uang"
          },
          "amount":{
            "en": "Amount",
            "id": "Jumlah"
          },
          "note":{
            "en": "Note",
            "id": "Keterangan"
          },
          "proceed":{
            "en": "Proceed",
            "id": "Proses"
          }
        },
        "sendBank":{
          "from":{
            "en": "From",
            "id": "Dari"
          },
          "bank":{
            "en": "Bank",
            "id": "Bank"
          },
          "account":{
            "en": "Account",
            "id": "Nomor Rekening"
          },
          "currency":{
            "en": "Currency",
            "id": "Mata Uang"
          },
          "amount":{
            "en": "Amount",
            "id": "Jumlah"
          },
          "note":{
            "en": "Note",
            "id": "Keterangan"
          },
          "proceed":{
            "en": "Proceed",
            "id": "Proses"
          }
        },
      }
    },
    "withdraw":{
      "tab":{
        "withdraw":{
          "en":"Withdraw",
          "id":"Penarikan"
        },
        "history":{
          "en":"History",
          "id":"Riwayat"
        }
      },
      "form":{
        "default":{
          "currency":{
            "en": "Currency",
            "id": "Mata Uang",
          },
          "bank":{
            "en": "Bank",
            "id": "Bank"
          },
          "account":{
            "en": "Account",
            "id": "Nomor Rekening"
          },
          "amount":{
            "en": "Amount",
            "id": "Jumlah",
          },
          "note":{
            "en": "Note",
            "id": "Keterangan"
          }
        }
      }
    },
    "mutation":{
      "tableHeader":{
        "date":{
          "en": "Date",
          "id": "Tanggal"
        },
        "type":{
          "en": "Transaction Type",
          "id": "Jenis Transaksi"
        },
        "currency":{
          "en": "currency",
          "id": "Mata Uang"
        },
        "receive":{
          "en": "Receive",
          "id": "Dana Masuk"
        },
        "expend":{
          "en": "Expend",
          "id": "Dana Keluar"
        },
        "status":{
          "en": "Status",
          "id": "Status"
        }
      },
      "form":{
        "dateFrom":{
          "en":"Date From",
          "id":"Dari Tanggal"
        },
        "dateTo":{
          "en":"To",
          "id":"Sampai"
        },
        "proceed":{
          "en":"Proceed",
          "id":"Proses"
        }
      }
    },
    "profile":{
      "view":{
        "userData":{
          "en":"User Data",
          "id":"Data Diri"
        },
        "sendMoneyAccount":{
          "en":"SendMoney Account ID",
          "id":"Nomor Akun SendMoney"
        },
        "name":{
          "en":"Name",
          "id":"Nama"
        },
        "birthday":{
          "en":"Birthday",
          "id":"Tanggal"
        },
        "email":{
          "en":"Email",
          "id":"Email"
        },
        "phone":{
          "en":"Phone Number",
          "id":"Nomor Telepon"
        },
        "verifyPhoneNumber":{
          "en":"Verify Phone Number",
          "id":"Verifikasi Nomor Telepon"
        },
        "addressData":{
          "en":"Address Data",
          "id":"Data Alamat"
        },
        "address":{
          "en":"Address",
          "id":"Alamat"
        },
        "city":{
          "en":"City",
          "id":"Kota"
        },
        "province":{
          "en":"Province",
          "id":"Provinsi"
        },
        "country":{
          "en":"Country",
          "id":"Negara"
        },
        "zipcode":{
          "en":"Zipcode",
          "id":"Kodepos"
        },
        "userDocument":{
          "en":"User Document",
          "id":"Dokumen Pengguna"
        },
        "identityType":{
          "en":"Identity Type",
          "id":"Jenis Identitas"
        },
        "status":{
          "id":"Status",
          "en":"Status"
        }
      },
    },
    "userDocument":{
      "form":{
        "identityVerification":{
          "en":"Identity Verification",
          "id":"Verifikasi Identitas"
        },
        "identityType":{
          "en":"Identity Type",
          "id":"Tipe Identitas"
        },
        "citizenId":{
          "en":"Citizen ID",
          "id":"KTP"
        },
        "driverLisence":{
          "en":"Driver Lisence",
          "id":"SIM"
        },
        "passport":{
          "en":"Passport",
          "id":"Passport"
        },
        "identityNumber":{
          "en":"Identity Number",
          "id":"Nomor Identitas"
        },
        "identityImage":{
          "en":"Identity Image",
          "id":"Foto Identitas"
        },
        "validity":{
          "en":"Validity",
          "id":"Masa Berlaku"
        },
        "note":{
          "en":"Note",
          "id":"Keterangan"
        },
        "upload":{
          "en":"Upload",
          "id":"Upload"
        }
      }
    },
    "userPhone":{
      "form":{
        "phoneVerification":{
          "en":"Phone Number Verification",
          "id":"Verifikasi Nomor Telepon",
        },
        "message":{
          "en":"We have sent you an sms",
          "id":"Silahkan periksa sms pada nomor telepon yang Anda Input untuk mendapatkan"
        },
        "verificationCode":{
          "en":"Verification Code",
          "id":"Kode Verifikasi"
        },
        "phoneNumber":{
          "en":"Phone Number",
          "id":"Nomor Telepon"
        },
        "validate":{
          "en":"Validate",
          "id":"Verifikasi"
        },
        "send":{
          "en":"Send",
          "id":"Kirim"
        }
      }
    },
    "currency":{
      "tableHeader":{
        "code":{
          "en":"Code",
          "id":"Kode"
        },
        "currency":{
          "en":"Currency",
          "id":"Mata Uang"
        },
        "status":{
          "en":"Status",
          "id":"Status"
        }
      }
    },
    "bank":{
      "tableHeader":{
        "code":{
          "en":"Code",
          "id":"Kode"
        },
        "name":{
          "en":"Name",
          "id":"Nama"
        },
        "currency":{
          "en":"Currency",
          "id":"Mata Uang"
        },
        "account":{
          "en":"Account",
          "id":"No.Rekening"
        },
        "user":{
          "en":"User",
          "id":"Atas Nama"
        },
        "status":{
          "en":"Status",
          "id":"Status"
        },
        "active":{
          "en":"Active",
          "id":"Aktif"
        },
        "inactive":{
          "en":"Inactive",
          "id":"Tidak Aktif"
        },
        "addBank":{
          "en":"Add Bank",
          "id":"Tambah Bank"
        }
      }
    },
    "security":{
      "text":{
        "activate":{
          "en":"Activate",
          "id":"Aktifkan"
        },
        "addSecurityLayer":{
          "en":"Add security layer when login",
          "id":"Keamanan Tambahan Saat Login"
        }
      },
      "response":{
        "disabled":{
          "en":"Security feature has been disabled",
          "id":"Fitur keamanan berhasil dimatikan"
        },
        "activate":{
          "en":"Security feature successfully activated",
          "id":"Fitur keamanan berhasil diaktifkan"
        }
      }
    },
    "changePassword":{
      "form":{
        "currentPassword":{
          "en":"Current Password",
          "id":"Password Saat ini"
        },
        "newPassword":{
          "en":"New Password",
          "id":"Password Baru"
        },
        "confirmPassword":{
          "en":"Confirm New Password",
          "id":"Konfirmasi Password Baru"
        },
        "update":{
          "en":"Update",
          "id":"Perbarui"
        }
      }
    },
    "addBank":{
      "form":{
        "bank":{
          "en":"Your Bank",
          "id":"Bank Anda"
        },
        "currency":{
          "en":"Currency",
          "id":"Mata Uang"
        },
        "user":{
          "en":"User",
          "id":"Atas Nama"
        },
        "account":{
          "en":"Account Number",
          "id":"Nomor Rekening"
        },
        "save":{
          "en":"Save",
          "id":"Simpan"
        }
      }
    },
    "forgetPassword":{
      "title":{
        "en":"Forget Password",
        "id":"Lupa Password"
      },
      "sendCodeBtn":{
        "en":"Send Verification Code",
        "id":"Kirim Kode Verifikasi"
      }
    }
  },
  menu:{
    "exchange":{
      "en":"Money Exchange",
      "id":"Konversi Saldo Mata Uang"
    },
    "deposit":{
      "en":"Top Up",
      "id":"Deposit"
    },
    "transfer":{
      "en":"Transfer",
      "id":"Kirim Dana"
    },
    "withdraw":{
      "en":"Withdraw",
      "id":"Penarikan"
    },
    "mutation":{
      "en":"Transaction Mutation",
      "id":"Mutasi Transaksi"
    },
    "profile":{
      "en":"Profile",
      "id":"Profil"
    },
    "setting":{
      "en":"Settings",
      "id":"Pengaturan"
    },
    "verification":{
      "en":"Identity Verification",
      "id":"Verifikasi Identitas"
    },
    "currency":{
      "en":"Currency",
      "id":"Mata Uang"
    },
    "bank":{
      "en":"Bank",
      "id":"Bank"
    },
    "securiy":{
      "en":"Security",
      "id":"Keamanan"
    },
    "changePassword":{
      "en":"Change Password",
      "id":"Ubah Password"
    },
    "addBank":{
      "en":"Add Bank",
      "id":"Tambah Bank"
    },
    "addressData":{
      "en":"User Address",
      "id":"Data Alamat"
    }
  },
  exchange:{
    "breadcrumb":{
      "en": "Currency Exchange",
      "id": "Konversi Saldo Mata Uang"
    },
    "button":{
      "en": "Proceed",
      "id": "Proses"
    }
  },
  response:{
    "transaction":{
      "success":{
        "en": "Transaction success",
        "id": "Transaksi berhasil"
      },
      "error":{
        "en": "Ups! Something wrong during transaction",
        "id": "Ups! Terjadi kesalahan saat transaksi"
      },
    },
    "saving":{
      "success":{
        "en": "Data succesfully stored",
        "id": "Data berhasil disimpan"
      },
      "error":{
        "en": "Ups! Something wrong when saving your data",
        "id": "Ups! Terjadi kesalahan saat menyimpan data Anda"
      },
    },
    "updating":{
      "success":{
        "en": "Data succesfully updated",
        "id": "Data berhasil diperbarui"
      },
      "error":{
        "en": "Ups! Something wrong when updating your data",
        "id": "Ups! Terjadi kesalahan saat memperbarui data Anda"
      },
    },
    "error":{
      "en": "Internal server error",
      "id": "Terjadi gangguan server"
    },
    "empty":{
      "id": "Tidak terjadi apa-apa",
      "en": "Seems nothing happend"
    },
    "emailOtp":{
      "success":{
        "en": "Please check your email, we have sent you verification code",
        "id": "Kami mengiri kode verifikasi melalui email"
      },
      "error":{
        "en": "We could not sent you the verification code, pleace check your email input",
        "id": "kami tidak dapat mengirim anda kode verifikasi, periksa kembali input email Anda"
      },
      "invalid":{
        "en": "Invalid code",
        "id": "Kode tidak cocok"
      },
      "resend":{
        "en": "Resend code",
        "id": "Kirim ulang" 
      }
    }
  },
  blankOptions:{
    "currency":{
      "id": "Pilih Mata Uang",
      "en": "Choose currency"
    },
    "bank":{
      "id": "Pilih Bank",
      "en": "Choose bank"
    },
    "paymentMethod":{
      "id": "Pilih Metode",
      "en": "Choose payment method"
    },
    "identityType":{
      "id":"Pilih tipe Identitas",
      "en":"Choose identity type",
    },
  }
}