export default {
  IDR: "id",
  USD: "us",
  CNY: "cn",
  JPY: "jp",
  SGD: "sg",
  EUR: 'eu',
  AUD: 'au',
  DKK: 'de',
  SEK: 'se',
  CAD: 'ca',
  CHF: 'ch',
  NZD: 'nz',
  GBP: 'gb',
  HKD: 'hk',
  SAR: 'sa',
  MYR: 'my',
  THB: 'th'
}