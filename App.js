import React from 'react';
import { StyleSheet, View } from 'react-native';
import SendMoney from './src/SendMoney';

//console.disableYellowBox = true;

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={ styles.statusBar }></View>
        <SendMoney />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBar:{
    backgroundColor: 'black',
    height:22,
  }
});
